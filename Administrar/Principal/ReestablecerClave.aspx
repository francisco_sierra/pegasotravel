﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaginaMaestra/Principal.Master" AutoEventWireup="true" CodeBehind="ReestablecerClave.aspx.cs" Inherits="PegasoTravel.Administrar.Principal.ReestablecerClave" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cph_cabecera" runat="server">
    <link href="../../CSS/styles.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_contenido" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table border="0" style="width: 100%">
                <tr>
                    <td style="width: 317px" align="left">
                        <asp:ImageButton ID="ImgRegresar" ImageUrl="~/Images/retro.png" Height="40px" Width="40px" runat="server" ToolTip="Regresar" OnClick="ImgRegresar_Click" CausesValidation="false" />
                        <br />
                        <asp:Label ID="Label5" runat="server" Text="Regresar" Font-Size="18px" Font-Bold="true" ForeColor="#122931"></asp:Label>
                    </td>

                </tr>
            </table>
            <table border="0px" align="center">
                <tr>
                    <td align="left">
                        <asp:Label ID="LblMensaje" runat="server" ForeColor="Red" Font-Size="14px" Font-Bold="true"></asp:Label>
                    </td>
                </tr>

                <tr>
                    <td align="right">
                        <asp:Label ID="Label1" runat="server" Text="Apellido:" ForeColor="#122931" Font-Size="12px"></asp:Label>
                    </td>
                    <td align="center">
                        <asp:TextBox ID="TxtApellidoB" runat="server" Height="16px" Style="margin-left: 0px" Width="163px" ForeColor="#122931" Font-Size="12px"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender runat="server" FilterType="UppercaseLetters,LowercaseLetters" TargetControlID="TxtApellidoB"></ajaxToolkit:FilteredTextBoxExtender>
                    </td>
                </tr>

                <tr>
                    <td align="right">
                        <asp:Label ID="Label2" runat="server" Text="E-mail:" ForeColor="#122931" Font-Size="12px"></asp:Label>
                    </td>
                    <td align="center">
                        <asp:TextBox ID="TxtEmailB" runat="server" Height="16px" Style="margin-left: 0px" Width="163px" ForeColor="#122931" Font-Size="12px"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="TxtEmailB" FilterType="UppercaseLetters, Numbers, LowercaseLetters, Custom" ValidChars="@._" runat="server"></ajaxToolkit:FilteredTextBoxExtender>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="Label3" runat="server" Text="Cédula:" ForeColor="#122931" Font-Size="12px"></asp:Label>
                    </td>
                    <td align="right">
                        <asp:TextBox ID="TxtCedulaB" runat="server" Height="16px" Style="margin-left: 0px" Width="163px" ForeColor="#122931" Font-Size="12px"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender runat="server" FilterType="Numbers" TargetControlID="TxtCedulaB"></ajaxToolkit:FilteredTextBoxExtender>
                    </td>
                    <td align="lefth" style="width: 38px">
                        <asp:ImageButton ID="ImgBusqueda" ImageUrl="~/Images/buscar.png" runat="server" Height="40px" Width="40px" ToolTip="Buscar" CausesValidation="false" OnClick="ImgBusqueda_Click" />
                    </td>
                </tr>
            </table>
            <table border="0px" align="center">
                <tr>
                    <td align="center">
                        <asp:GridView ID="Gdv_ListaUsuario" runat="server" AutoGenerateColumns="false" BorderColor="#DEDFDE" BorderStyle="Double" BorderWidth="1px" OnRowCommand="UsuarioRowCommand">
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                    <ItemTemplate>
                                        <asp:ImageButton ID="ImgEditar" runat="server" CausesValidation="false" ToolTip="Seleccionar" CommandArgument='<%#Eval("usu_codigo") %>' CommandName="Editar" Height="40px" ImageUrl="~/Images/select.png" Width="40px" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Cedula">
                                    <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px"/>
                                    <ItemTemplate>
                                        <asp:Label ID="LblCedula" runat="server" Text='<%#Eval("usu_cedula") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Nombre">
                                    <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                    <ItemTemplate>
                                        <asp:Label ID="LblNombre" runat="server" Text='<%#Eval("usu_nombre") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Apellido">
                                    <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px"/>
                                    <ItemTemplate>
                                        <asp:Label ID="LblApellido" runat="server" Text='<%#Eval("usu_apellido") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
            <table border="0px" align="center">
                <tr>
                    <td class="style11">
                        <asp:Label ID="LblApellido" runat="server" Text="Apellido:" ForeColor="#122931" Font-Size="12px"></asp:Label>
                    </td>
                    <td class="style14">
                        <asp:TextBox ID="TxtApellido" runat="server" Width="140px" ForeColor="#122931" Font-Size="12px"></asp:TextBox>
                    </td>
                    <td class="style11">
                        <asp:Label ID="LblNombre" runat="server" Text="Nombre:" ForeColor="#122931" Font-Size="12px"></asp:Label>
                    </td>
                    <td class="style14">
                        <asp:TextBox ID="TxtNombre" runat="server" Width="140px" ForeColor="#122931" Font-Size="12px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="style11">
                        <asp:Label ID="LblCedula" runat="server" Text="Cédula:" ForeColor="#122931" Font-Size="12px"></asp:Label>
                    </td>
                    <td class="style14">
                        <asp:TextBox ID="TxtCedula" runat="server" Width="140px" ForeColor="#122931" Font-Size="12px"></asp:TextBox>
                    </td>
                    <td class="style11">
                        <asp:Label ID="LblMail" runat="server" Text="E-mail: " ForeColor="#122931" Font-Size="12px"></asp:Label>
                    </td>
                    <td class="style14">
                        <asp:TextBox ID="TxtMail" runat="server" Width="140px" ForeColor="#122931" Font-Size="12px"></asp:TextBox>
                    </td>
                </tr>
            </table>
            <table border="0px" align="center">
                <tr>
                    <td>
                        <asp:Button ID="BtnGuardar" runat="server" OnClick="BtnGuardar_Click" Text="Restablecer" BackColor="#122931"
                                        ForeColor="White" Height="37px" Width="130px" Font-Bold="true" Font-Size="18px" />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
