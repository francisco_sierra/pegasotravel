﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaginaMaestra/Principal.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="PegasoTravel.Administrar.Principal.Login" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cph_cabecera" runat="server">
    <link href="../../CSS/styles.css" rel="stylesheet" />
    <link href="../../CSS/login.css" rel="stylesheet" />
    <style type="text/css">
        .auto-style6 {
            width: 340px;
            height: 20px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_contenido" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <%-- <table align="center">
                <tr>
                    <td align="left" class="auto-style6">
                        <asp:Label ID="Lblmensaje" runat="server" ForeColor="Red" Font-Size="12px" Font-Bold="true"></asp:Label>
                    </td>
                </tr>
            </table>
            <table border="0" align="center" style="border: groove; border-color: midnightblue; background-image: inherit;">
                <tr>
                    <td>
                        <table border="0" align="center">
                            <tr>
                                <td style="background: #122931; width: 340px" align="center">

                                    <asp:Label ID="Label1" runat="server" Text="INICIAR SESIÓN" ForeColor="White" Font-Bold="true" Font-Size="18px"></asp:Label>
                                    <br />
                                    <asp:Label ID="Label2" runat="server" Text="Por favor ingrese los datos correctos." ForeColor="White" Font-Size="12px"></asp:Label>
                                </td>
                            </tr>
                        </table>
                        <table border="0" align="center">

                            <tr>
                                <td style="width: 120px" align="left">
                                    <asp:Label ID="Label3" runat="server" Text="Correo:" ForeColor="#122931" Font-Size="12px"></asp:Label>
                                </td>
                                <td style="width: 185px">
                                    <asp:TextBox ID="Txt_login" runat="server" Width="217px" MaxLength="2048" ForeColor="#122931" Font-Size="12px" ToolTip="Ingrese su correo"></asp:TextBox>
                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="Txt_login" FilterType="Custom, Numbers, LowercaseLetters" ValidChars=".@_" runat="server"></cc1:FilteredTextBoxExtender>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 120px" align="left">
                                    <asp:Label ID="Label4" runat="server" Text="Contraseña:" ForeColor="#122931" Font-Size="12px"></asp:Label>
                                </td>
                                <td style="width: 21px">
                                    <asp:TextBox ID="Txt_pass" runat="server" TextMode="Password" Width="217px" MaxLength="16" ForeColor="#122931" Font-Size="12px" ToolTip="Ingrese su contraseña"></asp:TextBox>
                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" TargetControlID="Txt_pass" FilterType="UppercaseLetters, Numbers, LowercaseLetters"  runat="server"></cc1:FilteredTextBoxExtender>
                                   
                                      </td>
                            </tr>
                            <tr>
                                <td align="center" style="width: 120px">
                                    <asp:Image ID="Image1" ImageUrl="~/Images/login_100128.png" Width="63px" Height="40px" runat="server" />
                                </td>
                                <td align="center">
                                    <asp:Button ID="Btn_ingreso" runat="server" Text="Ingresar" BackColor="#122931"
                                        ForeColor="White" Height="37px" Width="130px" Font-Bold="true" Font-Size="18px"
                                        OnClick="Btn_ingreso_Click" />
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 120px"></td>
                                <td align="right">
                                    <asp:LinkButton ID="LnkOlvido" runat="server" Text="Olvide mi contraseña" ForeColor="Red" OnClick="LnkOlvido_Click"></asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>--%>
            <table align="center">
                <tr>
                    <td align="left" class="auto-style6">
                        <asp:Label ID="Lblmensaje" runat="server" ForeColor="Red" Font-Size="14px" Font-Bold="true"></asp:Label>
                    </td>
                </tr>
            </table>
            <table border="0" align="center">

                <tr>
                    <td>
                        <%-- <div class="container">--%>
                        <%-- <div class="row">--%>
                        <div class="col-md-offset-5 col-md-3">
                            <div class="form-login">
                                <h4 style="color: #122931">INICIAR SESIÓN</h4>
                                <%--   <input type="text" id="userName" class="form-control input-sm chat-input" placeholder="username" />--%>
                                <asp:Label ID="Label3" runat="server" Text="Correo:" ForeColor="#122931" Font-Size="14px"></asp:Label><br />
                                <asp:TextBox ID="Txt_login" runat="server" Width="217px" MaxLength="2048" ForeColor="#122931" Font-Size="14px" ToolTip="Ingrese su correo"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="Txt_login" FilterType="Custom, Numbers, LowercaseLetters" ValidChars=".@_" runat="server"></cc1:FilteredTextBoxExtender>
                                <br /> <br />
                                <%-- <input type="text" id="userPassword" class="form-control input-sm chat-input" placeholder="password" />--%>
                                <asp:Label ID="Label4" runat="server" Text="Contraseña:" ForeColor="#122931" Font-Size="14px"></asp:Label><br />
                                <asp:TextBox ID="Txt_pass" runat="server" Font-Size="14px" ForeColor="#122931" MaxLength="16" TextMode="Password" ToolTip="Ingrese su contraseña" Width="217px"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="UppercaseLetters, Numbers, LowercaseLetters" TargetControlID="Txt_pass">
                                </cc1:FilteredTextBoxExtender>
                                 <br />
                                <div class="wrapper">
                                     <br />
                                    <span class="group-btn"><%--  <a href="#" class="btn btn-primary btn-md">login <i class="fa fa-sign-in"></i></a>--%>
                                        <asp:Button ID="Btn_ingreso" runat="server" BackColor="#122931" Font-Bold="true" Font-Size="18px" ForeColor="White" Height="37px" OnClick="Btn_ingreso_Click" Text="Ingresar" Width="130px" />
                                        <br />
                                        <br />
                                        <asp:LinkButton ID="LnkOlvido" runat="server" ForeColor="Red" Font-Bold="false" OnClick="LnkOlvido_Click" Text="Olvide mi contraseña"></asp:LinkButton>
                                    </span>
                                </div>


                            </div>

                        </div>
                        <%--     </div>--%>
                        <%-- </div>--%>

                    </td>
                </tr>
            </table>




        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
