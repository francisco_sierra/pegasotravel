﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaginaMaestra/Principal.Master" AutoEventWireup="true" CodeBehind="CambioClave.aspx.cs" Inherits="PegasoTravel.Administrar.Principal.CambioClave" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cph_cabecera" runat="server">
    <link href="../../CSS/styles.css" rel="stylesheet" />
    <link href="../../CSS/login.css" rel="stylesheet" />
    <style type="text/css">
        .auto-style1 {
            width: 179px;
        }
        .auto-style2 {
            width: 310px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_contenido" runat="server">
    <asp:ScriptManager ID="ScriptManager2" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <table style="width: 100%">
                <tr>
                    <td style="width: 264px" align="left">
                        <asp:ImageButton ID="ImgRegresar" ImageUrl="~/Images/retro.png" runat="server" OnClick="ImgRegresar_Click" Height="40px" Width="40px" CausesValidation="false" ToolTip="Regresar" />
                        <br />
                        <asp:Label ID="Label5" runat="server" Text="Regresar" Font-Size="18px" Font-Bold="true" ForeColor="#122931"></asp:Label>
                              </td>
                    <td align="right">
                        <asp:Label ID="Lbl_Login" runat="server" Font-Bold="true"  Font-Size="18px" ForeColor="#122931"></asp:Label>
                </tr>
            </table>
            <table align="center" class="auto-style2">
                <tr>
                    <td>
                        <asp:Label ID="Lblmensaje" runat="server" ForeColor="Red" Font-Bold="true" Font-Size="14px" ></asp:Label>
                    </td>
                </tr>
            </table>
           <%-- <table border="0" align="center" style="border:groove;border-color:midnightblue">
                <tr>
                    <td>
                         <table border="0" align="center">
                <tr>
                    <td style="background: #122931" align="center" class="auto-style2">
                        <asp:Label ID="Label1" runat="server" Text="Cambio de Clave" ForeColor="White" Font-Bold="true" Font-Size="18px"></asp:Label>
                        <br />
                        <asp:Label ID="Label2" runat="server" Text="Por favor ingrese la nueva clave." ForeColor="White" Font-Size="12px"></asp:Label>
                    </td>

                </tr>
            </table>
         <table border="0" align="center">

                <tr>
                    <td  align="left">
                        <asp:Label ID="Label3" runat="server" Text="Nueva Clave:" ForeColor="#122931" Font-Size="12px"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="Txt_calvenueva" runat="server" Width="200px"  MaxLength="16" TextMode="Password" ForeColor="#122931" Font-Size="12px" ToolTip="Ingrese la nueva clave" ></asp:TextBox>
                      
                           <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="Txt_calvenueva" FilterType="UppercaseLetters,Numbers,LowercaseLetters" runat="server"></ajaxToolkit:FilteredTextBoxExtender>
                        <ajaxToolkit:PasswordStrength ID="PasswordStrength1" TargetControlID="Txt_calvenueva" 
                            DisplayPosition="rightside"
                            MinimumNumericCharacters="2"
                            MinimumSymbolCharacters="2"
                            PreferredPasswordLength="10"
                            PrefixText="Fortaleza: "
                            RequiresUpperAndLowerCaseCharacters="true"
                            StrengthIndicatorType="Text"
                            TextStrengthDescriptions="muy débil; débil; mejorable; buena; perfecta"
                           
                            runat="server">
                        </ajaxToolkit:PasswordStrength>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:Label ID="Label4" runat="server" Text="Confirmar:" ForeColor="#122931" Font-Size="12px"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="Txt_confirmclave" runat="server" Width="200px" MaxLength="16" TextMode="Password" ForeColor="#122931" Font-Size="12px" ToolTip="Ingrese la misma clave que la anterior"></asp:TextBox>
                        <br />
                        <asp:CompareValidator ID="CompareValidator1" ControlToValidate="Txt_confirmclave" ControlToCompare="Txt_calvenueva" runat="server" ErrorMessage="Debe ser igual a la clave ingresada" ForeColor="Red" Font-Bold="true"></asp:CompareValidator>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" TargetControlID="Txt_confirmclave" FilterType="UppercaseLetters,Numbers,LowercaseLetters" runat="server"></ajaxToolkit:FilteredTextBoxExtender>
                    </td>
                </tr>
                <tr>
                    <td align="center" style="width: 94px">
                        <asp:Image ID="Image1" ImageUrl="~/Images/login_100128.png" Width="63px" Height="40px" runat="server" />
                    </td>
                    <td align="center">
                        <asp:Button ID="Btn_ingreso" runat="server" Text="Aceptar" BackColor="#122931"
                            ForeColor="White" Height="37px" Width="130px" Font-Bold="true" Font-Size="18px"
                            OnClick="Btn_ingreso_Click" />

                        <br />
                    </td>
                </tr>
            </table>
                    </td>
                </tr>
            </table>--%>
            <table border="0" align="center">

                <tr>
                    <td>
                        <%-- <div class="container">--%>
                        <%-- <div class="row">--%>
                        <div class="col-md-offset-5 col-md-3">
                            <div class="form-login">
                                <h4 style="color: #122931">CAMBIO DE CLAVE</h4>
                                <%--   <input type="text" id="userName" class="form-control input-sm chat-input" placeholder="username" />--%>
                               <asp:Label ID="Label3" runat="server" Text="Nueva Clave" ForeColor="#122931" Font-Size="14px"></asp:Label>
                   <br /> <asp:TextBox ID="Txt_calvenueva" runat="server" Width="200px"  MaxLength="16" TextMode="Password" ForeColor="#122931" Font-Size="14px" ToolTip="Ingrese la nueva clave" ></asp:TextBox>
                      
                           <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="Txt_calvenueva" FilterType="UppercaseLetters,Numbers,LowercaseLetters" runat="server"></ajaxToolkit:FilteredTextBoxExtender>
                        <ajaxToolkit:PasswordStrength ID="PasswordStrength1" TargetControlID="Txt_calvenueva" 
                            DisplayPosition="rightside"
                            MinimumNumericCharacters="2"
                            PreferredPasswordLength="8"
                            PrefixText="Fortaleza: "
                            RequiresUpperAndLowerCaseCharacters="true"
                            StrengthIndicatorType="Text"
                            TextStrengthDescriptions="débil; mejorable; buena; perfecta"
                           
                            runat="server">
                        </ajaxToolkit:PasswordStrength>        <br /> <br />
                                <%-- <input type="text" id="userPassword" class="form-control input-sm chat-input" placeholder="password" />--%>
                                     <asp:Label ID="Label4" runat="server" Text="Confirmar" ForeColor="#122931" Font-Size="14px"></asp:Label>
                   <br /><asp:TextBox ID="Txt_confirmclave" runat="server" Width="200px" MaxLength="16" TextMode="Password" ForeColor="#122931" Font-Size="14px" ToolTip="Ingrese la misma clave que la anterior"></asp:TextBox>
                        <br />
                        <asp:CompareValidator ID="CompareValidator1" ControlToValidate="Txt_confirmclave" ControlToCompare="Txt_calvenueva" runat="server" ErrorMessage="Debe ser igual a la clave ingresada" ForeColor="Red" Font-Bold="false"></asp:CompareValidator>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" TargetControlID="Txt_confirmclave" FilterType="UppercaseLetters,Numbers,LowercaseLetters" runat="server"></ajaxToolkit:FilteredTextBoxExtender>
                            <br />
                                <div class="wrapper">
                                     <br />
                                    <span class="group-btn"><%--  <a href="#" class="btn btn-primary btn-md">login <i class="fa fa-sign-in"></i></a>--%>
                                        <asp:Button ID="Btn_ingreso" runat="server" Text="Aceptar" BackColor="#122931"
                            ForeColor="White" Height="37px" Width="130px" Font-Bold="true" Font-Size="18px"
                            OnClick="Btn_ingreso_Click" />     </span>
                                </div>


                            </div>

                        </div>
                        <%--     </div>--%>
                        <%-- </div>--%>

                    </td>
                </tr>
            </table>
            
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

