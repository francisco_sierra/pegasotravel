﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaginaMaestra/Lista.Master" AutoEventWireup="true" CodeBehind="ListaFactura.aspx.cs" Inherits="PegasoTravel.Administrar.Reserva.ListaFactura" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cph_cabecera" runat="server">
    <link href="../../CSS/styles.css" rel="stylesheet" />
    <script language="jscript" type="text/javascript">
        function Seleccion(idfac, Numero, Estado) {
            var factura= {};
            factura.idfac = idfac;
            factura.Numero = Numero;
            factura.Estado = Estado;
            window.returnValue = factura;
            window.close();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_contenido" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table border="0" align="center">
                <tr>
                    <td>
                        <asp:Label ID="LblBusq" runat="server" Text="Buscar factura: " ForeColor="#122931" Font-Size="12px"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="TxtBusqueda" runat="server" Width="140px" ForeColor="#122931" Font-Size="12px"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="TxtBusqueda" FilterType="UppercaseLetters, Numbers, LowercaseLetters, Custom" ValidChars="-" runat="server"></ajaxToolkit:FilteredTextBoxExtender>
                          </td>
                    <td>
                        <asp:ImageButton ID="ImgBusqueda" ImageUrl="~/Images/buscar.png" OnClick="ImgBusqueda_click" ToolTip="Buscar" runat="server" Height="40px" Width="40px" />
                    </td>
                </tr>
            </table>
        <div style="overflow: auto; height: 200px;width:auto">
                <table border="0" align="center">
                    <tr>
                        <td Align="center">
                            <asp:GridView ID="Gdv_Lista" AutoGenerateColumns="false"
                                runat="server" BorderColor="#DEDFDE" BorderStyle="Double" BorderWidth="1px">
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px"/>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgSelecionar" runat="server" CausesValidation="false" CommandName="Seleccionar" Height="40px" ToolTip="Seleccionar" Width="40px"
                                                ImageUrl="~/Images/select.png" OnClientClick='<%# "Seleccion (" + Eval("fac_codigo") + ", \"" + Eval("fac_numero") + "\", \"" + Eval("fac_estado") +  "\" )" %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Número">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:Label ID="Lbl1" runat="server" Text='<%#Eval("fac_numero") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Neto">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:Label ID="LblN2" runat="server" Text='<%#Eval("fac_neto") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Comisión">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px"/>
                                        <ItemTemplate>
                                            <asp:Label ID="Lb3" runat="server" Text='<%#Eval("fac_comision") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Iva">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px"/>
                                        <ItemTemplate>
                                            <asp:Label ID="Lbl4" runat="server" Text='<%#Eval("fac_iva") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Total">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:Label ID="Lbl5" runat="server" Text='<%#Eval("fac_total") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
                </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
