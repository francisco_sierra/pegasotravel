﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaginaMaestra/Admin.Master" AutoEventWireup="true" CodeBehind="AerolineaNueva.aspx.cs" Inherits="PegasoTravel.Administrar.Reserva.AerolineaNueva" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cph_cabecera" runat="server">
    <link href="../../CSS/styles.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_contenido" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table style="width: 100%; text-align: center" border="0" align="center">
                <tr>
                    <td>
                        <asp:Label ID="Label2" runat="server" Text="AEROLÍNEA" Font-Size="24px" Font-Bold="true" ForeColor="#122931"></asp:Label>
                        <br />
                        <br />
                    </td>
                </tr>
            </table>
            <table style="width: 100%; text-align: center" border="0" align="center">
                <tr>
                    <td align="left">
                        <asp:Label ID="LblMensaje" runat="server" ForeColor="Red" Font-Size="14px" Font-Bold="true"></asp:Label>
                    </td>
                    <td align="right">
                        <asp:HiddenField ID="HdfUsu" runat="server" />
                    </td>
                </tr>
            </table>
            <table style="width: 100%" border="0" align="center">
                <tr>
                    <td align="center">
                        <asp:ImageButton ID="ImgNuevo" ImageUrl="~/Images/new.png" Height="40px" Width="40px" ToolTip="Agregar nueva aerolínea" runat="server" OnClick="ImgNuevo_Click" CausesValidation="false" />
                        <br />
                        <asp:Label ID="Label1" runat="server" Text="Agregar aerolínea" Font-Bold="true" ForeColor="#122931" Font-Size="14px"></asp:Label>
                    </td>
                </tr>
            </table>

            <table border="0" align="center">
                <tr>
                    <td>
                        <asp:Label ID="LblAerolinea" runat="server" Text="Aerolínea:" ForeColor="#122931" Font-Size="12px"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="TxtAerolinea" runat="server" Width="140px" ForeColor="#122931" Font-Size="12px"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="TxtAerolinea" FilterType="Custom, UppercaseLetters, LowercaseLetters" ValidChars="- _" runat="server"></ajaxToolkit:FilteredTextBoxExtender>
                    </td>
                    <td>
                        <asp:Label ID="LblPrefijo" runat="server" Text="Prefijo: " ForeColor="#122931" Font-Size="12px"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="TxtPrefijo" runat="server" Width="140px" ForeColor="#122931" Font-Size="12px"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" TargetControlID="TxtPrefijo" FilterType="Numbers, UppercaseLetters" runat="server"></ajaxToolkit:FilteredTextBoxExtender>
                    </td>
                </tr>
                <tr>
                    <td></td><td></td><td></td>
                    <td align="right">
                        <asp:Button ID="BtnGuardar" runat="server" OnClick="BtnGuardar_Click" BackColor="#122931"
                            ForeColor="White" Height="37px" Width="130px" Font-Bold="true" Font-Size="18px" Text="Guardar" />
                    </td>
                </tr>
                <tr>
                    <td></td><td></td><td></td>
                    <td align="right">
                        <asp:Button ID="BtnModificar" runat="server" OnClick="BtnModificar_Click" Text="Modificar" BackColor="#122931"
                            ForeColor="White" Height="37px" Width="130px" Font-Bold="true" Font-Size="18px" />
                    </td>
                </tr>
                </tr>
            </table>
            <br />
            <table border="0" align="center">
               <tr>
                    <td>
                        <asp:Label ID="Label3" runat="server" Text="Buscar aerolínea" Font-Bold="true" ForeColor="#122931" Font-Size="16px"></asp:Label>
                    </td>
                   </tr>
                   <tr>
                    <td>
                        <asp:Label ID="LblBusqueda" runat="server" Text="Ingrese el nombre de la Aerolínea: " ForeColor="#122931" Font-Size="12px"></asp:Label>
                        <asp:TextBox ID="TxtBusqueda" runat="server" Width="140px" ForeColor="#122931" Font-Size="12px"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" TargetControlID="TxtBusqueda" FilterType="Custom, UppercaseLetters, LowercaseLetters" ValidChars="- _" runat="server"></ajaxToolkit:FilteredTextBoxExtender>
                        <asp:ImageButton ID="ImgBusqueda" ImageUrl="~/Images/buscar.png" runat="server" Height="40px" Width="40px" ToolTip="Buscar" CausesValidation="false" OnClick="ImgBusqueda_Click" />
                    </td>
                </tr>
            </table>
            <br />
             <div style="overflow: auto; height: 200px;width:auto">
                <table style="width: 100%" align="center">
                    <tr>
                        <td align="center">
                            <asp:GridView ID="Gdv_Lista" AutoGenerateColumns="false" OnRowCommand="ListaRowCommand"
                                runat="server" BorderColor="#DEDFDE" BorderStyle="Double" BorderWidth="1px">
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" />
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImgEditar" CommandArgument='<%#Eval("aer_codigo") %>' CommandName="Editar" ToolTip="Editar datos" CausesValidation="false"
                                                Height="40px" Width="40px" ImageUrl="~/Images/Edit.png" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White"/>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImgEliminar" CommandArgument='<%#Eval("aer_codigo") %>' CommandName="Eliminar" CausesValidation="false" ToolTip="Desactivar aerolínea"
                                                Height="40px" Width="40px" ImageUrl="~/Images/delete.png" runat="server" OnClientClick="return confirm('Seguro que desea desactivar el registro')" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Aerolínea">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White"/>
                                        <ItemTemplate>
                                            <asp:Label ID="Lblaero" runat="server" Text='<%#Eval("aer_nombre") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Prefijo">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblnomen" runat="server" Text='<%#Eval("aer_prefijo") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
