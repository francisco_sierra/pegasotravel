﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaginaMaestra/Lista.Master" AutoEventWireup="true" CodeBehind="ListaReserva.aspx.cs" Inherits="PegasoTravel.Administrar.Reserva.ListaReserva" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cph_cabecera" runat="server">
    <link href="../../CSS/styles.css" rel="stylesheet" />
    <script language="jscript" type="text/javascript">
        function Seleccion(idres) {
            var reserva = {};
            reserva.idres = idres;
            window.returnValue = reserva;
            window.close();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_contenido" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table align="center" border="0">
                <tr>
                    <td>
                        <asp:Label ID="LblBusqueda" runat="server" Text="Buscar por:" ForeColor="#122931" Font-Size="12px"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="DdlBusqueda" runat="server" ForeColor="#122931" Font-Size="12px" ToolTip="Seleccionar el tipo de búsqueda">
                            <asp:ListItem Text="Seleccione Busqueda" Value="0"></asp:ListItem>
                            <asp:ListItem Text="Cliente" Value="c"></asp:ListItem>
                            <asp:ListItem Text="Guia" Value="g"></asp:ListItem>
                            <asp:ListItem Text="Ruta" Value="r"></asp:ListItem>
                            <asp:ListItem Text="Factura" Value="f"></asp:ListItem>
                            <asp:ListItem Text="Gds" Value="gd"></asp:ListItem>
                            <asp:ListItem Text="Consolidadora" Value="co"></asp:ListItem>
                            <asp:ListItem Text="Estado" Value="e"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:TextBox ID="TxtBusqueda" runat="server" Width="140px" ForeColor="#122931" Font-Size="12px"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="TxtBusqueda" FilterType="UppercaseLetters, Numbers, LowercaseLetters, Custom" ValidChars="-" runat="server"></ajaxToolkit:FilteredTextBoxExtender>
                    </td>
                    <td>
                        <asp:ImageButton ID="ImgBusqueda" ImageUrl="~/Images/buscar.png" OnClick="ImgBusqueda_click" ToolTip="Buscar" runat="server" Height="40px" Width="40px" />
                    </td>
                </tr>
            </table>
            <div style="overflow: auto; height: 200px;width:auto">
                <table border="0" align="center">
                    <tr>
                        <td align="center">
                            <asp:GridView ID="Gdv_Lista" AutoGenerateColumns="false"
                                runat="server" BorderColor="#DEDFDE" BorderStyle="Double" BorderWidth="1px">
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px"  />
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgSelecionar" runat="server" CausesValidation="false" ToolTip="Seleccionar" CommandName="Seleccionar" Height="40px" Width="40px"
                                                ImageUrl="~/Images/select.png" OnClientClick='<%# "Seleccion (" + Eval("dres_codigo") + " )" %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Cliente">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px"  />
                                        <ItemTemplate>
                                            <asp:Label ID="lblclientes" runat="server" Text='<%#Eval("TBL_CLIENTE.cli_nombre") + " " + Eval("TBL_CLIENTE.cli_apellido") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Ruta">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px"  />
                                        <ItemTemplate>
                                            <asp:Label ID="Lblruti" runat="server" Text='<%#Eval("TBL_RUTA.rut_nombre") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Aerolinea">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px"  />
                                        <ItemTemplate>
                                            <asp:Label ID="Lblaro" runat="server" Text='<%#Eval("TBL_AEROLINEA.aer_prefijo") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Gds">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:Label ID="Lblpg" runat="server" Text='<%#Eval("TBL_GDS.gds_nombre") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Consolidadora">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:Label ID="Lblconso" runat="server" Text='<%#Eval("TBL_CONSOLIDADORA.con_prefijo") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Fecha Salida">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:Label ID="Lblfsal" runat="server" Text='<%#Eval("dres_fecha_salida") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Fecha Retorno">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:Label ID="Lblfret" runat="server" Text='<%#Eval("dres_fecha_retorno") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Factura">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:Label ID="Lblfactn" runat="server" Text='<%#Eval("TBL_FACTURA.fac_numero") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Total">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:Label ID="Lbltotf" runat="server" Text='<%#Eval("TBL_FACTURA.fac_total") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Estado">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:Label ID="Lblpref" runat="server" Text='<%#Eval("TBL_ESTADO.est_nombre") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Usuario">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:Label ID="Lbltoma" runat="server" Text='<%#Eval("TBL_USUARIO.usu_nombre")+ " " + Eval("TBL_USUARIO.usu_apellido") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
