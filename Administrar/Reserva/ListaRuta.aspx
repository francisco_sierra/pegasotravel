﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaginaMaestra/Lista.Master" AutoEventWireup="true" CodeBehind="ListaRuta.aspx.cs" Inherits="PegasoTravel.Administrar.Reserva.ListaRuta" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cph_cabecera" runat="server">
    <link href="../../CSS/styles.css" rel="stylesheet" />
     <script language="jscript" type="text/javascript">
        function Seleccion(idrut, Nombre) {
            var ruta = {};
            ruta.idrut = idrut;
            ruta.Nombre = Nombre;
            window.returnValue = ruta;
            window.close();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_contenido" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table border="0" align="center">
                <tr>
                    <td>
                        <asp:Label ID="LblBusqueda" runat="server" Text="Buscar ruta:" ForeColor="#122931" Font-Size="12px"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="TxtBusqueda" runat="server" Width="140px" ForeColor="#122931" Font-Size="12px"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="TxtBusqueda" FilterType="Custom, UppercaseLetters, LowercaseLetters" ValidChars="- _" runat="server"></ajaxToolkit:FilteredTextBoxExtender>
                          </td>
                    <td>
                        <asp:ImageButton ID="ImgBusqueda" ImageUrl="~/Images/buscar.png" OnClick="ImgBusqueda_click" ToolTip="Buscar" runat="server" Height="40px" Width="40px" />
                    </td>
                </tr>
            </table>
         <div style="overflow: auto; height: 200px;width:auto">
                <table border="0px" align="center">
                    <tr>
                        <td align="center">
                            <asp:GridView ID="Gdv_Lista" AutoGenerateColumns="false"
                                runat="server" BorderColor="#DEDFDE" BorderStyle="Double" BorderWidth="1px">
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px"  />
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgSelecionar" runat="server" CausesValidation="false" ToolTip="Seleccionar" CommandName="Seleccionar" Height="40px" Width="40px"
                                                ImageUrl="~/Images/select.png" OnClientClick='<%# "Seleccion (" + Eval("rut_codigo") + ", \"" + Eval("rut_nombre") + "\" )" %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Ruta">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px"  />
                                        <ItemTemplate>
                                            <asp:Label ID="LblNombre" runat="server" Text='<%#Eval("rut_nombre") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
