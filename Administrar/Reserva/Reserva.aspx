﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaginaMaestra/Admin.Master" AutoEventWireup="true" CodeBehind="Reserva.aspx.cs" Inherits="PegasoTravel.Administrar.Reserva.Reserva" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cph_cabecera" runat="server">
    <link href="../../CSS/styles.css" rel="stylesheet" />


    <script src="../../JS/Functions.js" type="text/javascript"></script>

    <link href="../../CSS/bootstrap.css" rel="stylesheet" />
    <style type="text/css">
        .modalBackground {
            background-color: Black;
            filter: alpha(opacity=90);
            opacity: 0.8;
            z-index: 10000;
        }
    </style>
    <script language="javascript" type="text/javascript">

   

        <%-- function ven_cliente() {
            var url = "../../Administrar/Cliente/ListaCliente.aspx?window=1";
            var cliente = openDialogWindow(url, 600, 400);
            if (cliente != null) {
                var idcli = $("<%=hdf_idcli.ClientID%>");
                var Nombre = $("<%=TxtNomCliente.ClientID%>");
                var Apellido = $("<%=TxtApeCliente.ClientID%>");
                var Telefono = $("<%=TxtTelCliente.ClientID%>");
                idcli.value = cliente.idcli;
                Nombre.value = cliente.Nombre;
                Apellido.value = cliente.Apellido;
                Telefono.value = cliente.Telefono;
           
            }

            return false;
       }

        function ven_ruta() {
            var url = "../../Administrar/Reserva/ListaRuta.aspx?window=1";
            var ruta = openDialogWindow(url, 600, 400);
            if (ruta != null) {
                var idrut = $("<%=hdfRuta.ClientID%>");
                var Nombre = $("<%=TxTRuta.ClientID%>");
                idrut.value = ruta.idrut;
                Nombre.value = ruta.Nombre;
               
            }

            return false;
        }
        function ven_aerolinea() {
            var url = "../../Administrar/Reserva/ListaAerolinea.aspx?window=1";
            var aerolinea = openDialogWindow(url, 600, 400);
            if (aerolinea != null) {
                var idaer = $("<%=hdfaeroli.ClientID%>");
                var Prefijo = $("<%=TxtAerolinea.ClientID%>");
                idaer.value = aerolinea.idaer;
                Prefijo.value = aerolinea.Prefijo;
            }

            return false;
        }
        function ven_gds() {
            var url = "../../Administrar/Reserva/GdsLista.aspx?window=1";
            var gds = openDialogWindow(url, 600, 400);
            if (gds != null) {
                var idgds = $("<%=hdfGds.ClientID%>");
                var Nombre = $("<%=TxtGds.ClientID%>");
                idgds.value = gds.idgds;
                Nombre.value = gds.Nombre;
            }

            return false;
        }
        function ven_consolidadora() {
            var url = "../../Administrar/Reserva/ListaConsolidadora.aspx?window=1";
            var consolidadora = openDialogWindow(url, 600, 400);
            if (consolidadora != null) {
                var idcon = $("<%=hdfConsol.ClientID%>");
                var Prefijo = $("<%=TxtConsolidadora.ClientID%>");
                idcon.value = consolidadora.idcon;
                Prefijo.value = consolidadora.Prefijo;
            }

            return false;
        }
        function ven_factura() {
            var url = "../../Administrar/Reserva/ListaFactura.aspx?window=1";
            var factura = openDialogWindow(url, 600, 400);
            if (factura != null) {
                var idfac = $("<%=hdfFac.ClientID%>");
                var Numero = $("<%=TxtFactura.ClientID%>");
                var Estado = $("<%=TxtEstado.ClientID%>");
                idfac.value = factura.idfac;
                Numero.value = factura.Numero;
                Estado.value = factura.Estado;
            }

            return false;
        }
        function ven_usuario() {
            var url = "../../Administrar/Usuario/ListaUsuario.aspx?window=1";
            var usuario = openDialogWindow(url, 600, 400);
            if (usuario != null) {
                var idusu = $("<%=hdfIdUsu.ClientID%>");
                
                var Apellido = $("<%=TxtUsuApe.ClientID%>");
                idusu.value = usuario.idusu;
                Nombre.value = usuario.Nombre;
                Apellido.value = usuario.Apellido;
            }
            return false;
        }--%>
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_contenido" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table style="width: 100%; text-align: center" border="0" align="center">
                <tr>
                    <td>
                        <asp:Label ID="Label2" runat="server" Text="RESERVA" Font-Size="24px" Font-Bold="true" ForeColor="#122931"></asp:Label>
                        <br />
                        <br />
                    </td>
                </tr>
            </table>
            <table style="width: 100%; text-align: center" border="0" align="center">
                <tr>
                    <td align="left">
                        <asp:Label ID="LblMensaje" runat="server" ForeColor="Red" Font-Size="14px" Font-Bold="true"></asp:Label>
                    </td>
                    <td align="right">
                        <asp:HiddenField ID="HdfUsu" runat="server" />
                    </td>
                </tr>
            </table>
            <table border="0" align="center">
                <tr>
                    <td align="center">
                        <asp:ImageButton ID="ImgNuevo" ImageUrl="~/Images/new.png"
                            OnClick="ImgNuevo_click" runat="server" Height="40px" Width="40px" ToolTip="Agregar nueva reserva" CausesValidation="false" />
                        <br />
                        <asp:Label ID="LblNuevo" runat="server" Text="Nueva Reserva" Font-Bold="true" ForeColor="#122931" Font-Size="12px"></asp:Label>
                    </td>
                    <td align="center">
                        <asp:ImageButton ID="ImgGuardar" ImageUrl="~/Images/save.png"
                            OnClick="ImgGuardar_click" runat="server" Height="40px" Width="40px" ToolTip="Guardar la reserva" />
                        <br />
                        <asp:Label ID="LblGuardar" runat="server" Text="Guardar" Font-Bold="true" ForeColor="#122931" Font-Size="12px"></asp:Label>
                    </td>
                    <td align="center">
                        <asp:ImageButton ID="ImgModificar" ImageUrl="~/Images/save.png"
                            OnClick="ImgModificar_click" runat="server" Height="40px" Width="40px" ToolTip="Modificar el estado de la reserva" />
                        <br />
                        <asp:Label ID="LblModificar" runat="server" Text="Modificar" Font-Bold="true" ForeColor="#122931" Font-Size="12px"></asp:Label>
                    </td>
                </tr>
            </table>
            <br />
            <table align="center" border="0">
                <tr>
                    <td>
                        <asp:Label ID="LblFechaReg" runat="server" Text="Fecha de registro:" ForeColor="#122931" Font-Size="12px"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="LblFechaRegistro" runat="server" ForeColor="#122931" Font-Size="12px"></asp:Label>
                    </td>
                    <td></td>
                    <td>
                        <asp:Label ID="LblUsuarioReg" runat="server" Text="Usuario que registra:" ForeColor="#122931" Font-Size="12px"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="LblUsuarioRegistro" runat="server" ForeColor="#122931" Font-Size="12px"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LblCli" runat="server" Text="Cliente:" ForeColor="#122931" Font-Size="12px"></asp:Label>
                    </td>
                    <td>
                        <%--<asp:TextBox ID="TxtNomCliente" runat="server" Width="140px" ForeColor="#122931" Font-Size="12px"></asp:TextBox>
                        <br />--%>
                        <asp:TextBox ID="TxtApeCliente" runat="server" Width="140px" ForeColor="#122931" Font-Size="12px"></asp:TextBox>
                        <br />
                        <asp:TextBox ID="TxtTelCliente" runat="server" Width="140px" ForeColor="#122931" Font-Size="12px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:ImageButton ID="ImgCliente" runat="server" CausesValidation="false" Height="40px" ToolTip="Buscar cliente" ImageUrl="~/Images/buscar.png" Width="40px" OnClick="ImgCliente_Click" />

                        <asp:HiddenField ID="hdf_idcli" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LblFechaSalida" runat="server" Text="Fecha Salida:" ForeColor="#122931" Font-Size="12px"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="TxtFechaSalida" runat="server" Width="140px" ForeColor="#122931" Font-Size="12px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:ImageButton ID="ImgFechaSalida" ImageUrl="~/Images/calendar.png" CausesValidation="false" runat="server" Height="40px" Width="40px" ToolTip="Seleccionar fecha de salida " />
                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" Format="dd/MM/yyyy" PopupPosition="TopRight" PopupButtonID="ImgFechaSalida" Enabled="true" TargetControlID="TxtFechaSalida" runat="server"></ajaxToolkit:CalendarExtender>
                    </td>
                    <td>
                        <asp:Label ID="LblFechaRetorno" runat="server" Text="Fecha Retorno:" ForeColor="#122931" Font-Size="12px"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="TxtFechaRetorno" runat="server" Width="140px" ForeColor="#122931" Font-Size="12px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:ImageButton ID="ImgFechaRetorno" ImageUrl="~/Images/calendar.png" CausesValidation="false" runat="server" Height="40px" Width="40px" ToolTip="Seleccionar fecha de retorno" />
                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" TargetControlID="TxtFechaRetorno" PopupPosition="TopRight" Format="dd/MM/yyyy" Enabled="true" ViewStateMode="Enabled" PopupButtonID="ImgFechaRetorno" runat="server"></ajaxToolkit:CalendarExtender>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LblRut" runat="server" Text="Ruta:" ForeColor="#122931" Font-Size="12px"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="TxTRuta" runat="server" Width="140px" ForeColor="#122931" Font-Size="12px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:ImageButton ID="ImgRuta" runat="server" CausesValidation="false" Height="40px" ImageUrl="~/Images/buscar.png" OnClick="ImgRuta_click" Width="40px" ToolTip="Buscar ruta" />

                        <asp:HiddenField ID="hdfRuta" runat="server" />
                    </td>
                    <td>
                        <asp:Label ID="LblAeroli" runat="server" Text="Aerolínea:" ForeColor="#122931" Font-Size="12px"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="TxtAerolinea" runat="server" Width="140px" ForeColor="#122931" Font-Size="12px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:ImageButton ID="ImgAerolinea" runat="server" CausesValidation="false" Height="40px" ImageUrl="~/Images/buscar.png" OnClick="ImgAerolinea_click" ToolTip="Buscar aerolínea" Width="40px" />

                        <asp:HiddenField ID="hdfaeroli" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LblGds" runat="server" Text="GDS: " ForeColor="#122931" Font-Size="12px"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="TxtGds" runat="server" Width="140px" ForeColor="#122931" Font-Size="12px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:ImageButton ID="ImgGds" runat="server" CausesValidation="false" Height="40px" ImageUrl="~/Images/buscar.png" OnClick="ImgGds_click" ToolTip="Buscar gds" Width="40px" />

                        <asp:HiddenField ID="hdfGds" runat="server" />
                    </td>
                    <td>
                        <asp:Label ID="LblConsolidadora" runat="server" Text="Consolidadora: " ForeColor="#122931" Font-Size="12px"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="TxtConsolidadora" runat="server" Width="140px" ForeColor="#122931" Font-Size="12px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:ImageButton ID="ImgConsolidadora" runat="server" CausesValidation="false" Height="40px" ImageUrl="~/Images/buscar.png" ToolTip="Buscar consolidadora" OnClick="ImgConsolidadora_click" Width="40px" />

                        <asp:HiddenField ID="hdfConsol" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LblFactura" runat="server" Text="Factura: " ForeColor="#122931" Font-Size="12px"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="TxtFactura" runat="server" Width="140px" ForeColor="#122931" Font-Size="12px"></asp:TextBox>
                        <br />
                        <asp:TextBox ID="TxtEstado" runat="server" Width="140px" ForeColor="#122931" Font-Size="12px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:ImageButton ID="ImgFactura" runat="server" CausesValidation="false" Height="40px" ImageUrl="~/Images/buscar.png" ToolTip="Buscar factura" OnClick="ImgFactura_click" Width="40px" />

                        <asp:HiddenField ID="hdfFac" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LblUsuToma" runat="server" Text="Toma la reserva: " ForeColor="#122931" Font-Size="12px"></asp:Label>
                    </td>
                    <td>
                        <%--  <asp:TextBox ID="TxtUsuNom" runat="server" Width="140px" ForeColor="#122931" Font-Size="12px"></asp:TextBox>
                        <br />--%>
                        <asp:TextBox ID="TxtUsuApe" runat="server" Width="140px" ForeColor="#122931" Font-Size="12px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:ImageButton ID="ImgUsuario" runat="server" CausesValidation="false" Height="40px" ImageUrl="~/Images/buscar.png" ToolTip="Buscar usuario que gestionó la reserva" OnClick="ImgUsuario_click" Width="40px" />

                        <asp:HiddenField ID="hdfIdUsu" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LblEstado" runat="server" Text="Estado: " ForeColor="#122931" Font-Size="12px"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="Ddl_Estado" runat="server" Width="140px" ForeColor="#122931" Font-Size="12px" ToolTip="Seleccionar el estado de la reserva"></asp:DropDownList>
                    </td>
                    <td>
                        <asp:Button ID="BtnAgregar" runat="server" OnClick="Agregar_Click" BackColor="#122931"
                            ForeColor="White" Height="37px" Width="130px" Font-Bold="true" Font-Size="18px"
                            Text="Agregar" />
                    </td>
                </tr>

            </table>
            <div style="overflow: auto; height: 200px; width: auto">
                <table border="0" align="center">
                    <tr>
                        <td align="center">
                            <asp:GridView ID="Gdv_Detalle" AutoGenerateColumns="false" OnRowCommand="DetalleRowCommand"
                                BorderColor="#DEDFDE" BorderStyle="Double" BorderWidth="1px"
                                runat="server" Height="158px">
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImgEliminar" CommandName="Eliminar" CausesValidation="false" ToolTip="Eliminar el registro" CommandArgument='<%#Bind("IdFactura") %>'
                                                Height="40px" Width="40px" ImageUrl="~/Images/delete.png" runat="server" OnClientClick="return confirm('Seguro que desea eliminar el registro')" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Cliente">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblclientes" runat="server" Text='<%#Eval("Cliente") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Ruta">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:Label ID="Lblruti" runat="server" Text='<%#Eval("Ruta") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Aerolinea">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:Label ID="Lblaro" runat="server" Text='<%#Eval("Aerolinea") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Gds">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:Label ID="Lblpg" runat="server" Text='<%#Eval("Gds") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Consolidadora">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:Label ID="Lblconso" runat="server" Text='<%#Eval("Consolidadora") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Fecha Salida" SortExpression="Fecha Salida">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:Label ID="Lblfsal" runat="server" Text='<%#Eval("FechaSalida", "{0:d}") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Fecha Retorno" SortExpression="Fecha Retorno">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:Label ID="Lblfret" runat="server" Text='<%#Eval("FechaRetorno", "{0:d}") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Factura">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:Label ID="Lblfactn" runat="server" Text='<%#Eval("Factura") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Total">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:Label ID="Lbltotf" runat="server" Text='<%#Eval("Total") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Estado">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:Label ID="Lblestr" runat="server" Text='<%#Eval("EstadoRes") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Usuario">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:Label ID="Lbltoma" runat="server" Text='<%#Eval("Usuario") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>
            <table align="center" border="0">
                <tr>
                    <td>
                        <asp:Label ID="Label1" runat="server" Text="Buscar Reserva" Font-Bold="true" ForeColor="#122931" Font-Size="16px"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LblBusqueda" runat="server" Text="Buscar por:" ForeColor="#122931" Font-Size="12px"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="DdlBusqueda" runat="server" ForeColor="#122931" Font-Size="12px" ToolTip="Seleccionar el tipo de búsqueda">
                            <asp:ListItem Text="Seleccione Busqueda" Value="0"></asp:ListItem>
                            <asp:ListItem Text="Cliente" Value="c"></asp:ListItem>
                            <asp:ListItem Text="Guia" Value="g"></asp:ListItem>
                            <asp:ListItem Text="Ruta" Value="r"></asp:ListItem>
                            <asp:ListItem Text="Factura" Value="f"></asp:ListItem>
                            <asp:ListItem Text="Gds" Value="gd"></asp:ListItem>
                            <asp:ListItem Text="Consolidadora" Value="co"></asp:ListItem>
                            <asp:ListItem Text="Estado" Value="e"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:TextBox ID="TxtBusqueda" runat="server" Width="140px" ForeColor="#122931" Font-Size="12px"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="TxtBusqueda" FilterType="Numbers, Custom, UppercaseLetters, LowercaseLetters" ValidChars="- _" runat="server"></ajaxToolkit:FilteredTextBoxExtender>
                    </td>
                    <td>
                        <asp:ImageButton ID="ImgBusqueda" ImageUrl="~/Images/buscar.png" OnClick="ImgBusqueda_click" runat="server" Height="40px" Width="40px" ToolTip="Buscar" />
                    </td>
                </tr>
            </table>
            <div style="overflow: auto; height: 200px; width: auto">
                <table align="center" border="0" style="text-align: center">
                    <tr>
                        <td align="center">
                            <asp:GridView ID="Gdv_Lista" AutoGenerateColumns="false" OnRowCommand="ReservaRowCommand"
                                runat="server" BorderColor="#DEDFDE" BorderStyle="Double" BorderWidth="1px">
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImgEditar" CommandArgument='<%#Eval("dres_codigo") %>' CommandName="Editar" CausesValidation="false" ToolTip="Editar el estado de reserva"
                                                Height="40px" Width="40px" ImageUrl="~/Images/Edit.png" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Cliente">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblclientes" runat="server" ForeColor="#122931" Font-Size="12px" Text='<%#Eval("TBL_CLIENTE.cli_nombre") + " " + Eval("TBL_CLIENTE.cli_apellido") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Ruta">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:Label ID="Lblruti" runat="server" Text='<%#Eval("TBL_RUTA.rut_nombre") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Aerolinea">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:Label ID="Lblaro" runat="server" Text='<%#Eval("TBL_AEROLINEA.aer_prefijo") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Gds">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:Label ID="Lblpg" runat="server" Text='<%#Eval("TBL_GDS.gds_nombre") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Consolidadora">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:Label ID="Lblconso" runat="server" Text='<%#Eval("TBL_CONSOLIDADORA.con_prefijo") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Fecha Salida" SortExpression="Fecha Salida">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:Label ID="Lblfsal" runat="server" Text='<%#Eval("dres_fecha_salida", "{0:d}") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Fecha Retorno" SortExpression="Fecha Retorno">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:Label ID="Lblfret" runat="server" Text='<%#Eval("dres_fecha_retorno", "{0:d}") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Factura">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:Label ID="Lblfactn" runat="server" Text='<%#Eval("TBL_FACTURA.fac_numero") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Total">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:Label ID="Lbltotf" runat="server" Text='<%#Eval("TBL_FACTURA.fac_total") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Estado">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:Label ID="Lblpref" runat="server" Text='<%#Eval("TBL_ESTADO.est_nombre") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Usuario">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:Label ID="Lbltoma" runat="server" Text='<%#Eval("TBL_USUARIO.usu_nombre")+ " " + Eval("TBL_USUARIO.usu_apellido") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Button ID="btnInicial" runat="server" Text="Button" Style="display: none" />
    <asp:Button ID="btnInicial1" runat="server" Text="Button" Style="display: none" />
    <asp:Button ID="btnInicial2" runat="server" Text="Button" Style="display: none" />
    <asp:Button ID="btnInicial3" runat="server" Text="Button" Style="display: none" />
    <asp:Button ID="btnInicial4" runat="server" Text="Button" Style="display: none" />
    <asp:Button ID="btnInicial5" runat="server" Text="Button" Style="display: none" />
    <asp:Button ID="btnInicial6" runat="server" Text="Button" Style="display: none" />
    <%--<ajaxToolkit:ModalPopupExtender BackgroundCssClass="modalBackground" PopupControlID="PanelModal" 
        ID="btnPopUp_ModalPopupExtender" runat="server" DynamicServicePath="" Enabled="True" TargetControlID="btnInicial">
    </ajaxToolkit:ModalPopupExtender>--%>

    <ajaxToolkit:ModalPopupExtender ID="ImgCliente_ModalPopupExtender" BackgroundCssClass="modalBackground" PopupControlID="PanelModal"
        runat="server" DynamicServicePath="" Enabled="True" TargetControlID="btnInicial">
    </ajaxToolkit:ModalPopupExtender>

    <asp:Panel ID="PanelModal" runat="server" Style="display: none; background: white; width: 40%; height: auto">
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <table border="0" style="width: 100%">
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="ImgSalir" runat="server" ImageUrl="~/Images/delete.png" Width="30px" Height="30px" OnClick="ImgSalir_Click" />
                        </td>
                    </tr>
                </table>
                <table border="0" align="center">
                    <tr>
                        <td>
                            <asp:Label ID="Label3" runat="server" Text="Buscar por:" ForeColor="#122931" Font-Size="12px"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="DropDownList1" runat="server" ForeColor="#122931" Font-Size="12px" ToolTip="Seleccionar el tipo de búsqueda">
                                <asp:ListItem Text="Seleccione Busqueda" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Apellido" Value="a"></asp:ListItem>
                                <asp:ListItem Text="Cedula" Value="c"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <asp:TextBox ID="TxtBusca" runat="server" Width="140px" ForeColor="#122931" Font-Size="12px" CausesValidation="false"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" TargetControlID="TxtBusca" FilterType="Numbers, Custom, UppercaseLetters, LowercaseLetters" ValidChars="- _" runat="server"></ajaxToolkit:FilteredTextBoxExtender>
                        </td>
                        <td>
                            <asp:ImageButton ID="ImgBuscaCli" ImageUrl="~/Images/buscar.png" OnClick="ImgBuscaCli_click" runat="server" Height="40px" Width="40px" ToolTip="Buscar" />
                        </td>
                    </tr>
                </table>
                <div style="overflow: auto; height: 200px; width: auto">
                    <table border="0" align="center">
                        <tr>
                            <td align="center">
                                <asp:GridView ID="GdvCliente" AutoGenerateColumns="false" OnRowCommand="Cliente"
                                    runat="server" BorderColor="#DEDFDE" BorderStyle="Double" BorderWidth="1px">
                                    <Columns>
                                        <asp:TemplateField>
                                            <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgSelecionar" runat="server" CausesValidation="false" CommandName="Seleccionar" CommandArgument='<%#Eval("cli_codigo") %>' ToolTip="Seleccionar" Height="40px" Width="40px"
                                                    ImageUrl="~/Images/select.png" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Nombre">
                                            <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                            <ItemTemplate>
                                                <asp:Label ID="LblNombre" runat="server" Text='<%#Eval("cli_nombre") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Apellido">
                                            <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                            <ItemTemplate>
                                                <asp:Label ID="LblAp" runat="server" Text='<%#Eval("cli_apellido") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Cédula">
                                            <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                            <ItemTemplate>
                                                <asp:Label ID="Lbldoc" runat="server" Text='<%#Eval("cli_documento") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </div>
            </ContentTemplate>

        </asp:UpdatePanel>
    </asp:Panel>

    <ajaxToolkit:ModalPopupExtender ID="ImgRuta_ModalPopupExtender" BackgroundCssClass="modalBackground" PopupControlID="PanelModal1"
        runat="server" DynamicServicePath="" Enabled="True" TargetControlID="btnInicial1">
    </ajaxToolkit:ModalPopupExtender>

    <asp:Panel ID="PanelModal1" runat="server" Style="display: none; background: white; width: 40%; height: auto">
        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
            <ContentTemplate>

                <%--  --%>
                <table border="0" style="width: 100%">
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="ImgSalir1" runat="server" ImageUrl="~/Images/delete.png" Width="30px" Height="30px" OnClick="ImgSalir1_Click" />
                        </td>
                    </tr>
                </table>
                <table border="0" align="center">
                    <tr>
                        <td>
                            <asp:Label ID="Label4" runat="server" Text="Buscar ruta:" ForeColor="#122931" Font-Size="12px"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="TxtBuscaRuta" runat="server" Width="140px" ForeColor="#122931" Font-Size="12px"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" TargetControlID="TxtBuscaRuta" FilterType="Custom, UppercaseLetters, LowercaseLetters" ValidChars="- _" runat="server"></ajaxToolkit:FilteredTextBoxExtender>
                        </td>
                        <td>
                            <asp:ImageButton ID="ImgBuscaRuta" ImageUrl="~/Images/buscar.png" OnClick="ImgBuscaRuta_click" ToolTip="Buscar" runat="server" Height="40px" Width="40px" />
                        </td>
                    </tr>
                </table>
                <div style="overflow: auto; height: 200px; width: auto">
                    <table border="0px" align="center">
                        <tr>
                            <td align="center">
                                <asp:GridView ID="GdvRuta" AutoGenerateColumns="false" OnRowCommand="Ruta"
                                    runat="server" BorderColor="#DEDFDE" BorderStyle="Double" BorderWidth="1px">
                                    <Columns>
                                        <asp:TemplateField>
                                            <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgSelecionar" runat="server" CausesValidation="false" CommandName="Seleccionar" CommandArgument='<%#Eval("rut_codigo") %>' ToolTip="Seleccionar" Height="40px" Width="40px"
                                                    ImageUrl="~/Images/select.png" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Ruta">
                                            <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                            <ItemTemplate>
                                                <asp:Label ID="LblNombre" runat="server" Text='<%#Eval("rut_nombre") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>

    <ajaxToolkit:ModalPopupExtender ID="ImgAerolinea_ModalPopupExtender" BackgroundCssClass="modalBackground" PopupControlID="PanelModal2"
        runat="server" DynamicServicePath="" Enabled="True" TargetControlID="btnInicial2">
    </ajaxToolkit:ModalPopupExtender>

    <asp:Panel ID="PanelModal2" runat="server" Style="display: none; background: white; width: 40%; height: auto">
        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
            <ContentTemplate>

                <%--  --%>
                <table border="0" style="width: 100%">
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="ImgSalir2" runat="server" ImageUrl="~/Images/delete.png" Width="30px" Height="30px" OnClick="ImgSalir2_Click" />
                        </td>
                    </tr>
                </table>
                <table border="0px" align="center">
                    <tr>
                        <td>
                            <asp:Label ID="Label5" runat="server" Text="Buscar por:" ForeColor="#122931" Font-Size="12px"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="DropDownList2" runat="server" ForeColor="#122931" Font-Size="12px" ToolTip="Seleccionar el tipo de búsqueda">
                                <asp:ListItem Text="Seleccione Busqueda" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Nombre" Value="n"></asp:ListItem>
                                <asp:ListItem Text="Prefijo" Value="p"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td style="width: 152px">
                            <asp:TextBox ID="TxtBuscaAerolinea" runat="server" Width="140px" ForeColor="#122931" Font-Size="12px"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" TargetControlID="TxtBuscaAerolinea" FilterType="Numbers, Custom, UppercaseLetters, LowercaseLetters" ValidChars="- _" runat="server"></ajaxToolkit:FilteredTextBoxExtender>
                        </td>
                        <td>
                            <asp:ImageButton ID="ImgBucaAerolinea" ImageUrl="~/Images/buscar.png" OnClick="ImgBucaAerolinea_click" runat="server" ToolTip="Buscar" Height="40px" Width="40px" />
                        </td>
                    </tr>
                </table>
                <div style="overflow: auto; height: 200px; width: auto">
                    <table border="0px" align="center">
                        <tr>
                            <td align="center">
                                <asp:GridView ID="GdvAerolinea" AutoGenerateColumns="false" OnRowCommand="Aerolinea"
                                    runat="server" BorderColor="#DEDFDE" BorderStyle="Double" BorderWidth="1px">
                                    <Columns>
                                        <asp:TemplateField>
                                            <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgSelecionar" runat="server" CausesValidation="false" CommandName="Seleccionar" CommandArgument='<%#Eval("aer_codigo") %>' ToolTip="Seleccionar" Height="40px" Width="40px"
                                                    ImageUrl="~/Images/select.png" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Aerolínea">
                                            <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                            <ItemTemplate>
                                                <asp:Label ID="LblNombre" runat="server" Text='<%#Eval("aer_nombre") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Prefijo">
                                            <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                            <ItemTemplate>
                                                <asp:Label ID="LblApellido" runat="server" Text='<%#Eval("aer_prefijo") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>

    <ajaxToolkit:ModalPopupExtender ID="ImgGds_ModalPopupExtender" BackgroundCssClass="modalBackground" PopupControlID="PanelModal3"
        runat="server" DynamicServicePath="" Enabled="True" TargetControlID="btnInicial3">
    </ajaxToolkit:ModalPopupExtender>

    <asp:Panel ID="PanelModal3" runat="server" Style="display: none; background: white; width: 40%; height: auto">
        <asp:UpdatePanel ID="UpdatePanel5" runat="server">
            <ContentTemplate>

                <%--  --%>
                <table border="0" style="width: 100%">
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="ImgSalir3" runat="server" ImageUrl="~/Images/delete.png" Width="30px" Height="30px" OnClick="ImgSalir3_Click" />
                        </td>
                    </tr>
                </table>
                <table border="0" align="center">
                    <tr>
                        <td>
                            <asp:Label ID="LblBusq" runat="server" Text="Buscar GDS: " ForeColor="#122931" Font-Size="12px"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="TxtBuscaGds" runat="server" Width="140px" ForeColor="#122931" Font-Size="12px"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" TargetControlID="TxtBuscaGds" FilterType="UppercaseLetters, LowercaseLetters" runat="server"></ajaxToolkit:FilteredTextBoxExtender>
                        </td>
                        <td>
                            <asp:ImageButton ID="ImgBuscaGds" ImageUrl="~/Images/buscar.png" OnClick="ImgBuscaGds_click" runat="server" ToolTip="Buscar" Height="40px" Width="40px" />
                        </td>
                    </tr>
                </table>
                <div style="overflow: auto; height: 200px; width: auto">
                    <table border="0" align="center">
                        <tr>
                            <td align="center">
                                <asp:GridView ID="GdvGds" AutoGenerateColumns="false" OnRowCommand="GDS"
                                    runat="server" BorderColor="#DEDFDE" BorderStyle="Double" BorderWidth="1px">
                                    <Columns>
                                        <asp:TemplateField>
                                            <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgSelecionar" runat="server" CausesValidation="false" CommandName="Seleccionar" CommandArgument='<%#Eval("gds_codigo") %>' ToolTip="Seleccionar" Height="40px" Width="40px"
                                                    ImageUrl="~/Images/select.png" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="GDS">
                                            <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                            <ItemTemplate>
                                                <asp:Label ID="LblNombre" runat="server" Text='<%#Eval("gds_nombre") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>

    <ajaxToolkit:ModalPopupExtender ID="ImgConsolidadora_ModalPopupExtender" BackgroundCssClass="modalBackground" PopupControlID="PanelModal4"
        runat="server" DynamicServicePath="" Enabled="True" TargetControlID="btnInicial4">
    </ajaxToolkit:ModalPopupExtender>

    <asp:Panel ID="PanelModal4" runat="server" Style="display: none; background: white; width: 40%; height: auto">
        <asp:UpdatePanel ID="UpdatePanel6" runat="server">
            <ContentTemplate>

                <%--  --%>
                <table border="0" style="width: 100%">
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="ImgSalir4" runat="server" ImageUrl="~/Images/delete.png" Width="30px" Height="30px" OnClick="ImgSalir4_Click" />
                        </td>
                    </tr>
                </table>
                <table border="0" align="center">
                    <tr>
                        <td>
                            <asp:Label ID="Label6" runat="server" Text="Buscar: " ForeColor="#122931" Font-Size="12px"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="TxtBuscaConsolidadora" runat="server" Width="140px" ForeColor="#122931" Font-Size="12px"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" TargetControlID="TxtBuscaConsolidadora" FilterType="UppercaseLetters, LowercaseLetters" runat="server"></ajaxToolkit:FilteredTextBoxExtender>
                        </td>
                        <td>
                            <asp:ImageButton ID="ImgBuscaConsolidadora" ImageUrl="~/Images/buscar.png" OnClick="ImgBuscaConsolidadora_click" ToolTip="Buscar" runat="server" Height="40px" Width="40px" />
                        </td>
                    </tr>
                </table>
                <div style="overflow: auto; height: 200px; width: auto">
                    <table border="0" align="center">
                        <tr>
                            <td align="center">
                                <asp:GridView ID="GdvConsolidadora" AutoGenerateColumns="false" OnRowCommand="Consolidadora"
                                    runat="server" BorderColor="#DEDFDE" BorderStyle="Double" BorderWidth="1px">
                                    <Columns>
                                        <asp:TemplateField>
                                            <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgSelecionar" runat="server" CausesValidation="false" CommandName="Seleccionar" CommandArgument='<%#Eval("con_codigo") %>' ToolTip="Seleccionar" Height="40px" Width="40px"
                                                    ImageUrl="~/Images/select.png" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Consolidadora">
                                            <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                            <ItemTemplate>
                                                <asp:Label ID="LblNombre" runat="server" Text='<%#Eval("con_prefijo") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>

    <ajaxToolkit:ModalPopupExtender ID="ImgFactura_ModalPopupExtender" BackgroundCssClass="modalBackground" PopupControlID="PanelModal5"
        runat="server" DynamicServicePath="" Enabled="True" TargetControlID="btnInicial5">
    </ajaxToolkit:ModalPopupExtender>

    <asp:Panel ID="PanelModal5" runat="server" Style="display: none; background: white; width: 40%; height: auto">
        <asp:UpdatePanel ID="UpdatePanel7" runat="server">
            <ContentTemplate>

                <%--  --%>
                <table border="0" style="width: 100%">
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="ImgSalir5" runat="server" ImageUrl="~/Images/delete.png" Width="30px" Height="30px" OnClick="ImgSalir5_Click" />
                        </td>
                    </tr>
                </table>
                <table border="0" align="center">
                    <tr>
                        <td>
                            <asp:Label ID="Label7" runat="server" Text="Buscar factura: " ForeColor="#122931" Font-Size="12px"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="TxtBuscaFactura" runat="server" Width="140px" ForeColor="#122931" Font-Size="12px"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" TargetControlID="TxtBuscaFactura" FilterType="UppercaseLetters, Numbers, LowercaseLetters, Custom" ValidChars="-" runat="server"></ajaxToolkit:FilteredTextBoxExtender>
                        </td>
                        <td>
                            <asp:ImageButton ID="ImgBuscaFactura" ImageUrl="~/Images/buscar.png" OnClick="ImgBuscaFactura_click" ToolTip="Buscar" runat="server" Height="40px" Width="40px" />
                        </td>
                    </tr>
                </table>
                <div style="overflow: auto; height: 200px; width: auto">
                    <table border="0" align="center">
                        <tr>
                            <td align="center">
                                <asp:GridView ID="GdvFactura" AutoGenerateColumns="false" OnRowCommand="Factura"
                                    runat="server" BorderColor="#DEDFDE" BorderStyle="Double" BorderWidth="1px">
                                    <Columns>
                                        <asp:TemplateField>
                                            <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgSelecionar" runat="server" CausesValidation="false" CommandName="Seleccionar" CommandArgument='<%#Eval("fac_codigo") %>' ToolTip="Seleccionar" Height="40px" Width="40px"
                                                    ImageUrl="~/Images/select.png" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Número">
                                            <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                            <ItemTemplate>
                                                <asp:Label ID="Lbl1" runat="server" Text='<%#Eval("fac_numero") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Neto">
                                            <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                            <ItemTemplate>
                                                <asp:Label ID="LblN2" runat="server" Text='<%#Eval("fac_neto") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Comisión">
                                            <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                            <ItemTemplate>
                                                <asp:Label ID="Lb3" runat="server" Text='<%#Eval("fac_comision") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Iva">
                                            <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                            <ItemTemplate>
                                                <asp:Label ID="Lbl4" runat="server" Text='<%#Eval("fac_iva") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Total">
                                            <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                            <ItemTemplate>
                                                <asp:Label ID="Lbl5" runat="server" Text='<%#Eval("fac_total") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>

    <ajaxToolkit:ModalPopupExtender ID="ImgUsuario_ModalPopupExtender" BackgroundCssClass="modalBackground" PopupControlID="PanelModal6"
        runat="server" DynamicServicePath="" Enabled="True" TargetControlID="btnInicial6">
    </ajaxToolkit:ModalPopupExtender>

    <asp:Panel ID="PanelModal6" runat="server" Style="display: none; background: white; width: 40%; height: auto">
        <asp:UpdatePanel ID="UpdatePanel8" runat="server">
            <ContentTemplate>

                <%--  --%>
                <table border="0" style="width: 100%">
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="ImgSalir6" runat="server" ImageUrl="~/Images/delete.png" Width="30px" Height="30px" OnClick="ImgSalir6_Click" />
                        </td>
                    </tr>
                </table>
                <table border="0" align="center">
                    <tr>
                        <td>
                            <asp:Label ID="Label8" runat="server" Text="Buscar por:" ForeColor="#122931" Font-Size="12px"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="DropDownList3" runat="server" ForeColor="#122931" Font-Size="12px" ToolTip="Seleccionar tipo de búsqueda">
                                <asp:ListItem Text="Seleccione Busqueda" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Apellido" Value="a"></asp:ListItem>
                                <asp:ListItem Text="Cedula" Value="c"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <asp:TextBox ID="TxtBuscaUsuario" runat="server" Width="140px" CausesValidation="false" ForeColor="#122931" Font-Size="12px"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" TargetControlID="TxtBuscaUsuario" FilterType="Numbers, Custom, UppercaseLetters, LowercaseLetters" ValidChars="- _" runat="server"></ajaxToolkit:FilteredTextBoxExtender>
                        </td>
                        <td>
                            <asp:ImageButton ID="ImgBuscaUsuario" ImageUrl="~/Images/buscar.png" OnClick="ImgBuscaUsuario_click" runat="server" ToolTip="Buscar" Height="40px" Width="40px" />
                        </td>
                    </tr>
                </table>
                <div style="overflow: auto; height: 200px; width: auto">
                    <table border="0" align="center">
                        <tr>
                            <td align="center">
                                <asp:GridView ID="GdvUsuario" AutoGenerateColumns="false" OnRowCommand="Usuario"
                                    runat="server" BorderColor="#DEDFDE" BorderStyle="Double" BorderWidth="1px">
                                    <Columns>
                                        <asp:TemplateField>
                                            <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgSelecionar" runat="server" CausesValidation="false" CommandName="Seleccionar" CommandArgument='<%#Eval("usu_codigo") %>' ToolTip="Seleccionar" Height="40px" Width="40px"
                                                    ImageUrl="~/Images/select.png" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Nombre">
                                            <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                            <ItemTemplate>
                                                <asp:Label ID="LblNombre" runat="server" Text='<%#Eval("usu_nombre") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Apellido">
                                            <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                            <ItemTemplate>
                                                <asp:Label ID="LblApellido" runat="server" Text='<%#Eval("usu_apellido") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>

    <%--  --%>
</asp:Content>
