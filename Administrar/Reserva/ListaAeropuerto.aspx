﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaginaMaestra/Lista.Master" AutoEventWireup="true" CodeBehind="ListaAeropuerto.aspx.cs" Inherits="PegasoTravel.Administrar.Reserva.ListaAeropuerto" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cph_cabecera" runat="server">
    <link href="../../CSS/styles.css" rel="stylesheet" />
    <%--  --%>
   

    <%--  --%>
    <script lang="javascript" type="text/javascript">
        function Seleccion(idaero, Nombre, Prefijo) {
            var aeropuerto = {};
            aeropuerto.idaero = idaero;
            aeropuerto.Nombre = Nombre;
            aeropuerto.Prefijo = Prefijo;
           // window.returnValue = aeropuerto;
            window.opener.document.forms = aeropuerto;
            window.close();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_contenido" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <%--  --%>
           

            <%--  --%>


            <table border="0" align="center">
                <tr>
                    <td>
                        <asp:Label ID="LblBusqueda" runat="server" Text="Buscar por:" ForeColor="#122931" Font-Size="12px"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="DdlBusqueda" runat="server" ForeColor="#122931" Font-Size="12px" ToolTip="Seleccionar el tipo de búsqueda">
                            <asp:ListItem Text="Seleccione Busqueda" Value="0"></asp:ListItem>
                            <asp:ListItem Text="Nombre" Value="n"></asp:ListItem>
                            <asp:ListItem Text="Prefijo" Value="p"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td style="width: 152px">
                        <asp:TextBox ID="TxtBusqueda" runat="server" Width="140px" ForeColor="#122931" Font-Size="12px"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="TxtBusqueda" FilterType="Custom, UppercaseLetters, LowercaseLetters" ValidChars="- _" runat="server"></ajaxToolkit:FilteredTextBoxExtender>
                    </td>
                    <td>
                        <asp:ImageButton ID="ImgBusqueda" ImageUrl="~/Images/buscar.png" OnClick="ImgBusqueda_click" ToolTip="Buscar" runat="server" Height="40px" Width="40px" />
                    </td>
                </tr>
            </table>
            <div style="overflow: auto; height: 200px; width: auto">
                <table border="0" align="center">
                    <tr>
                        <td align="center">
                            <asp:GridView ID="Gdv_Lista" AutoGenerateColumns="false"
                                runat="server" BorderColor="#DEDFDE" BorderStyle="Double" BorderWidth="1px">
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgSelecionar" runat="server" CausesValidation="false" CommandName="Seleccionar" ToolTip="Seleccionar" Height="40px" Width="40px"
                                                ImageUrl="~/Images/select.png" OnClientClick='<%# "Seleccion (" + Eval("aero_codigo") + ", \"" + Eval("aero_nombre") + "\", \"" + Eval("aero_prefijo") +  "\" )" %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Aeropuerto">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:Label ID="LblNombre" runat="server" Text='<%#Eval("aero_nombre") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Prefijo">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:Label ID="Lblpre" runat="server" Text='<%#Eval("aero_prefijo") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
