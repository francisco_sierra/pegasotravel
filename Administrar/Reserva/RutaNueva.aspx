﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaginaMaestra/Admin.Master" AutoEventWireup="true" CodeBehind="RutaNueva.aspx.cs" Inherits="PegasoTravel.Administrar.Reserva.RutaNueva" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cph_cabecera" runat="server">
    <link href="../../CSS/styles.css" rel="stylesheet" />

    <script src="../../JS/Functions.js"></script>

    <%--  --%>
    <link href="../../CSS/bootstrap.css" rel="stylesheet" />
    <style type="text/css">
        .modalBackground {
            background-color: Black;
            filter: alpha(opacity=90);
            opacity: 0.8;
            z-index: 10000;
        }
    </style>
    <%--  --%>

    <script lang="javascript" type="text/javascript">
        function ven_aeropuerto() {
            var url = "../../Administrar/Reserva/ListaAeropuerto.aspx?window=1";
            var aeropuerto = openDialogWindow(url, 800, 800);


            if (aeropuerto != null) {
                var idaero = $("<%=hdf_idaero.ClientID%>");
                var Nombre = $("<%=TxtAeropuerto.ClientID%>");
                var Prefijo = $("<%=TxtPrefijo.ClientID%>");
                idaero.value = aeropuerto.idaero;
                Nombre.value = aeropuerto.Nombre;
                Prefijo.value = aeropuerto.Prefijo;
            }
            return false;
        }
    </script>
    <style type="text/css">
        .auto-style1 {
            overflow: auto;
            width: 100%;
            height: 256px;
        }

        .auto-style2 {
            overflow: auto;
            width: 99%;
            height: 159px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_contenido" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <%--  --%>


            <%--  --%>
            <table style="width: 100%; text-align: center" border="0" align="center">
                <tr>
                    <td>
                        <asp:Label ID="Label2" runat="server" Text="RUTA" Font-Size="24px" Font-Bold="true" ForeColor="#122931"></asp:Label>
                        <br />
                        <br />
                    </td>
                </tr>
            </table>
            <table style="width: 100%; text-align: center" border="0" align="center">
                <tr>
                    <td align="left">
                        <asp:Label ID="LblMensaje" runat="server" ForeColor="Red" Font-Size="14px" Font-Bold="true"></asp:Label>
                    </td>
                    <td align="right">
                        <asp:HiddenField ID="HdfUsu" runat="server" />
                    </td>
                </tr>
            </table>
            <table border="0" align="center">
                <tr>
                    <td align="center">
                        <asp:ImageButton ID="ImgNuevo" ImageUrl="~/Images/new.png"
                            OnClick="ImgNuevo_click" runat="server" Height="40px" Width="40px" CausesValidation="false" ToolTip="Agregar nueva ruta" />
                        <br />
                        <asp:Label ID="LblAgregar" runat="server" Text="Agregar Ruta" Font-Bold="true" ForeColor="#122931" Font-Size="14px"></asp:Label>
                    </td>
                    <td align="center">
                        <asp:ImageButton ID="ImgGuardar" ImageUrl="~/Images/save.png"
                            OnClick="ImgGuardar_click" runat="server" Height="40px" Width="40px" ToolTip="Guardar ruta" />
                        <br />
                        <asp:Label ID="LblGuardar" runat="server" Text="Guardar" Font-Bold="true" ForeColor="#122931" Font-Size="14px"></asp:Label>
                    </td>
                </tr>
            </table>
            <table align="center" border="0">
                <tr>
                    <td>
                        <asp:Label ID="LblNombre" runat="server" Text="Nombre de ruta:" ForeColor="#122931" Font-Size="12px"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="TxtRuta" runat="server" Width="200px" ForeColor="#122931" Font-Size="12px"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="TxtRuta" FilterType="Custom, UppercaseLetters" ValidChars="- _" runat="server"></ajaxToolkit:FilteredTextBoxExtender>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Lblaeropuerto" runat="server" Text="Aeropuerto:" ForeColor="#122931" Font-Size="12px"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="TxtAeropuerto" Width="200px" runat="server" ForeColor="#122931" Font-Size="12px"></asp:TextBox>
                        <asp:TextBox ID="TxtPrefijo" Width="140px" runat="server" ForeColor="#122931" Font-Size="12px"></asp:TextBox>
                    </td>
                    <td class="style11">
                        <%--  --%>
                        <%-- <asp:Button ID="btnPopUp" runat="server" Height="47px" Text="MOSTRAR POPUP"
                            Width="258px" OnClick="btnPopUp_Click" />--%>

                        <%--  --%>
                        <asp:ImageButton ID="ImgAeropuerto" runat="server" CausesValidation="false" Height="40px"
                            ImageUrl="~/Images/buscar.png" ToolTip="Buscar aeropuerto" Width="40px" OnClick="ImgAeropuerto_Click" />



                        <asp:HiddenField ID="hdf_idaero" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="BtnAgregar" runat="server" OnClick="Agregar_Click" BackColor="#122931"
                            ForeColor="White" Height="37px" Width="130px" Font-Bold="true" Font-Size="18px"
                            Text="Agregar" />
                    </td>
                </tr>
            </table>
            <div style="overflow: auto; height: 200px; width: auto">
                <table border="0px" align="center">
                    <tr>
                        <td align="center">
                            <asp:GridView ID="Gdv_Detalle" AutoGenerateColumns="False" 
                                BorderColor="#DEDFDE" BorderStyle="Double" BorderWidth="1px" runat="server">
                                <%--  DataKeyNames="IdAeropuerto" OnRowCommand="DetalleRowCommand"  --%>

                                <Columns>
                                   <%-- <asp:TemplateField>
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImgEliminar" CommandName="Eliminar" CommandArgument='<%#Bind("IdAeropuerto") %>' CausesValidation="false" ToolTip="Eliminar aeropuerto"
                                                Height="40px" Width="40px" ImageUrl="~/Images/delete.png" runat="server" OnClientClick="return confirm('Seguro que desea eliminar el registro')" />
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                   <%-- <asp:TemplateField HeaderText="Aeropuerto">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblidAero" runat="server" Text='<%#Eval("IdAeropuerto") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                    <asp:TemplateField HeaderText="Aeropuerto">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblAero" runat="server" Text='<%#Eval("Aeropuerto") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Prefijo">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:Label ID="Lblpref" runat="server" Text='<%#Eval("Prefijo") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>

            <table align="center" border="0">
                <tr>
                    <td>
                        <asp:Label ID="Label1" runat="server" Text="Buscar Ruta" Font-Bold="true" ForeColor="#122931" Font-Size="16px"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LblBusqueda" runat="server" Text="Ingresar el prefijo de la Ruta:" ForeColor="#122931" Font-Size="12px"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="TxtBusqueda" runat="server" CausesValidation="false" Width="140px" ForeColor="#122931" Font-Size="12px"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Custom, UppercaseLetters, LowercaseLetters" ValidChars="- _" TargetControlID="TxtBusqueda">
                        </ajaxToolkit:FilteredTextBoxExtender>
                    </td>
                    <td>
                        <asp:ImageButton ID="ImgBusqueda" runat="server" Height="40px" ImageUrl="~/Images/buscar.png" ToolTip="Buscar" OnClick="ImgBusqueda_click" Width="40px" />
                    </td>
                </tr>
            </table>
            <div style="overflow: auto; width: 100%; height: 200px">
                <table border="0" align="center">
                    <tr>
                        <td>
                            <asp:GridView ID="Gdv_Lista" AutoGenerateColumns="false"
                                runat="server" BorderColor="#DEDFDE" BorderStyle="Double" BorderWidth="1px">
                                <Columns>
                                    <asp:TemplateField HeaderText="Ruta">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:Label ID="LblNombre" runat="server" Text='<%#Eval("rut_nombre") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>

                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <%--  --%>
    <asp:Button ID="btnInicial" runat="server" Text="Button" Style="display: none" />
    <%--<ajaxToolkit:ModalPopupExtender BackgroundCssClass="modalBackground" PopupControlID="PanelModal" 
        ID="btnPopUp_ModalPopupExtender" runat="server" DynamicServicePath="" Enabled="True" TargetControlID="btnInicial">
    </ajaxToolkit:ModalPopupExtender>--%>

    <ajaxToolkit:ModalPopupExtender ID="ImgAeropuerto_ModalPopupExtender" BackgroundCssClass="modalBackground" PopupControlID="PanelModal"
        runat="server" DynamicServicePath="" Enabled="True" TargetControlID="btnInicial">
    </ajaxToolkit:ModalPopupExtender>


    <asp:Panel ID="PanelModal" runat="server" Style="display: none; background: white; width: 40%; height: auto">
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <table border="0" style="width: 100%">
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="ImgSalir" runat="server" ImageUrl="~/Images/delete.png" Width="30px" Height="30px" OnClick="ImgSalir_Click" />
                        </td>
                    </tr>
                </table>
                <table border="0" align="center">
                    <tr>
                        <td>
                            <asp:Label ID="Label3" runat="server" Text="Buscar por:" ForeColor="#122931" Font-Size="12px"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="DdlBusqueda" runat="server" ForeColor="#122931" Font-Size="12px" ToolTip="Seleccionar el tipo de búsqueda">
                                <asp:ListItem Text="Seleccione Busqueda" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Nombre" Value="n"></asp:ListItem>
                                <asp:ListItem Text="Prefijo" Value="p"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td style="width: 152px">
                            <asp:TextBox ID="TxtBucaAero" runat="server" Width="140px" ForeColor="#122931" Font-Size="12px"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" TargetControlID="TxtBucaAero" FilterType="Custom, UppercaseLetters, LowercaseLetters" ValidChars="- _" runat="server"></ajaxToolkit:FilteredTextBoxExtender>
                        </td>
                        <td>
                            <asp:ImageButton ID="ImgBuscaAero" ImageUrl="~/Images/buscar.png" OnClick="ImgBuscaAero_click" ToolTip="Buscar" runat="server" Height="40px" Width="40px" />
                        </td>
                    </tr>
                </table>
                <div style="overflow: auto; height: 200px; width: auto">
                    <table border="0" align="center">
                        <tr>
                            <td align="center">
                                <asp:GridView ID="GdvAero" AutoGenerateColumns="false" OnRowCommand="Aero"
                                    runat="server" BorderColor="#DEDFDE" BorderStyle="Double" BorderWidth="1px">
                                    <Columns>
                                        <asp:TemplateField>
                                            <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgSelecionar" runat="server" CausesValidation="false" CommandName="Seleccionar" CommandArgument='<%#Eval("aero_codigo") %>' ToolTip="Seleccionar" Height="40px" Width="40px"
                                                    ImageUrl="~/Images/select.png" />
                                                <%--OnClientClick='<%# "Seleccion (" + Eval("aero_codigo") + ", \"" + Eval("aero_nombre") + "\", \"" + Eval("aero_prefijo") +  "\" )" %>'   --%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Aeropuerto">
                                            <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                            <ItemTemplate>
                                                <asp:Label ID="LblNombre" runat="server" Text='<%#Eval("aero_nombre") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Prefijo">
                                            <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                            <ItemTemplate>
                                                <asp:Label ID="Lblpre" runat="server" Text='<%#Eval("aero_prefijo") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </div>


            </ContentTemplate>

        </asp:UpdatePanel>
    </asp:Panel>
    <%--  --%>
</asp:Content>
