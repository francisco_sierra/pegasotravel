﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaginaMaestra/Lista.Master" AutoEventWireup="true" CodeBehind="GdsLista.aspx.cs" Inherits="PegasoTravel.Administrar.Reserva.GdsLista" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cph_cabecera" runat="server">
    <link href="../../CSS/styles.css" rel="stylesheet" />
     <script language="jscript" type="text/javascript">
        function Seleccion(idgds, Nombre) {
            var gds = {};
            gds.idgds = idgds;
            gds.Nombre = Nombre;
            window.returnValue = gds;
            window.close();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_contenido" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table border="0" align="center" >
                <tr>
                    <td>
                        <asp:Label ID="LblBusq" runat="server" Text="Buscar GDS: " ForeColor="#122931" Font-Size="12px"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="TxtBusqueda" runat="server" Width="140px" ForeColor="#122931" Font-Size="12px"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="TxtBusqueda" FilterType="UppercaseLetters, LowercaseLetters" runat="server"></ajaxToolkit:FilteredTextBoxExtender>
                         </td>
                    <td>
                        <asp:ImageButton ID="ImgBusqueda" ImageUrl="~/Images/buscar.png" OnClick="ImgBusqueda_click" runat="server" ToolTip="Buscar" Height="40px" Width="40px" />
                    </td>
                </tr>
            </table>
        <div style="overflow: auto; height: 200px;width:auto">
                <table border="0" align="center">
                    <tr>
                        <td align="center">
                            <asp:GridView ID="Gdv_Lista" AutoGenerateColumns="false" 
                                runat="server" BorderColor="#DEDFDE" BorderStyle="Double" BorderWidth="1px">
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgSelecionar" ToolTip="Seleccionar" runat="server" CausesValidation="false" CommandName="Seleccionar" Height="40px" Width="40px"
                                                ImageUrl="~/Images/select.png"  OnClientClick='<%# "Seleccion (" + Eval("gds_codigo") + ", \"" + Eval("gds_nombre") + "\")" %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="GDS">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px"/>
                                        <ItemTemplate>
                                            <asp:Label ID="LblNombre" runat="server" Text='<%#Eval("gds_nombre") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>