﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaginaMaestra/Admin.Master" AutoEventWireup="true" CodeBehind="FacturaNueva.aspx.cs" Inherits="PegasoTravel.Administrar.Reserva.FacturaNueva" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cph_cabecera" runat="server">
    <link href="../../CSS/styles.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_contenido" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table style="width: 100%; text-align: center" border="0" align="center">
                <tr>
                    <td>
                        <asp:Label ID="Label2" runat="server" Text="FACTURA" Font-Size="24px" Font-Bold="true" ForeColor="#122931"></asp:Label>
                        <br />
                        <br />
                    </td>
                </tr>
            </table>
            <table style="width: 100%; text-align: center" border="0" align="center">
                <tr>
                    <td align="left">
                        <asp:Label ID="LblMensaje" runat="server" ForeColor="Red" Font-Size="14px" Font-Bold="true"></asp:Label>
                    </td>
                    <td align="right">
                        <asp:HiddenField ID="HdfUsu" runat="server" />
                    </td>
                </tr>
            </table>
            <table style="width: 100%" border="0" align="center">
                <tr>
                    <td align="center">
                        <asp:ImageButton ID="ImgNuevo" ImageUrl="~/Images/new.png" Height="40px" Width="40px" ToolTip="agregar nueva factura" runat="server" OnClick="ImgNuevo_Click" CausesValidation="false" />
                        <br />
                        <asp:Label ID="Label1" runat="server" Text="Agregar Factura" Font-Bold="true" ForeColor="#122931" Font-Size="14px"></asp:Label>
                    </td>
                </tr>
            </table>
            <br />
            <table border="0" align="center">
                <tr>
                    <td>
                        <asp:Label ID="LblFactura" runat="server" Text="Número de Factura:" ForeColor="#122931" Font-Size="12px"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="TxtFactura" runat="server" Width="140px" ForeColor="#122931" Font-Size="12px"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" TargetControlID="TxtFactura" FilterType="UppercaseLetters, Numbers, Custom" ValidChars="-" runat="server"></ajaxToolkit:FilteredTextBoxExtender>
                    </td>
                    <td>
                        <asp:Label ID="LblFormaPago" runat="server" Text="Forma de pago:" ForeColor="#122931" Font-Size="12px"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="Ddl_FormaPago" runat="server" Width="140px" ForeColor="#122931" Font-Size="12px">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:Label ID="LblEstado" runat="server" Text="Estado:" ForeColor="#122931" Font-Size="12px"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="TxtEstado" runat="server" Width="140px" ForeColor="#122931" Font-Size="12px"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="TxtEstado" FilterType="Custom, UppercaseLetters, LowercaseLetters" ValidChars="- _" runat="server"></ajaxToolkit:FilteredTextBoxExtender>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LblNeto" runat="server" Text="Neto:" ForeColor="#122931" Font-Size="12px"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="TxtNeto" runat="server" Width="140px" ForeColor="#122931" Font-Size="12px"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" TargetControlID="TxtNeto" FilterType="Numbers, Custom" ValidChars="," runat="server"></ajaxToolkit:FilteredTextBoxExtender>
                            </td>
                    <td align="right">
                        <asp:Label ID="LblComision" runat="server" Text="FEE: " ForeColor="#122931" Font-Size="12px"></asp:Label>
                          </td>
                    <td>
                        <asp:TextBox ID="TxtComision" runat="server" Width="140px" ForeColor="#122931" Font-Size="12px"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" TargetControlID="TxtComision" FilterType="Numbers, Custom" ValidChars="," runat="server"></ajaxToolkit:FilteredTextBoxExtender>
                     <asp:Label ID="LblDescripcionFEE" runat="server" Text="?" ForeColor="red" Font-Bold="true" ToolTip="Comisión de la gestión de reserva" Font-Size="12px"></asp:Label>
                             
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LblIva" runat="server" Text="Iva:" ForeColor="#122931" Font-Size="12px"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="LblIvaCalculado" runat="server" ForeColor="#122931" Font-Size="12px"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LblTotal" runat="server" Text="Total:" ForeColor="#122931" Font-Size="12px"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="LblTotalCalculado" runat="server" ForeColor="#122931" Font-Size="12px"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td></td><td></td><td></td><td></td><td></td>
                    <td align="right">
                        <asp:Button ID="BtnGuardar" runat="server" OnClick="BtnGuardar_Click" BackColor="#122931"
                            ForeColor="White" Height="37px" Width="130px" Font-Bold="true" Font-Size="18px" Text="Guardar" />
                    </td>
                </tr>
                <tr>
                     <td></td><td></td><td></td><td></td><td></td>
                    <td align="right">
                        <asp:Button ID="BtnModificar" runat="server" OnClick="BtnModificar_Click" Text="Modificar" BackColor="#122931"
                            ForeColor="White" Height="37px" Width="130px" Font-Bold="true" Font-Size="18px" />
                    </td>
                </tr>
                </tr>
            </table>
            <br />
            <table border="0" align="center">
              <tr>
                    <td>
                        <asp:Label ID="Label3" runat="server" Text="Buscar Factura" Font-Bold="true" ForeColor="#122931" Font-Size="16px"></asp:Label>
                    </td>
                   </tr>
                    <tr>
                    <td>
                        <asp:Label ID="LblBusqueda" runat="server" Text="Ingrese el número de Factura: " ForeColor="#122931" Font-Size="12px"></asp:Label>
                        <asp:TextBox ID="TxtBusqueda" runat="server" Width="140px" ForeColor="#122931" Font-Size="12px"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" TargetControlID="TxtBusqueda" FilterType="Numbers, UppercaseLetters, Custom" ValidChars="-" runat="server"></ajaxToolkit:FilteredTextBoxExtender>
                        </td>
                        <td>
                         <asp:ImageButton ID="ImgBusqueda" ImageUrl="~/Images/buscar.png" runat="server" Height="40px" Width="40px" ToolTip="Buscar" CausesValidation="false" OnClick="ImgBusqueda_Click" />
                    </td>
                </tr>
            </table>
            <br />
            <div style="overflow: auto; height: 200px;width:auto">
                <table align="center">
                    <tr>
                        <td align="center">
                            <asp:GridView ID="Gdv_Lista" AutoGenerateColumns="false" OnRowCommand="ListaRowCommand"
                                runat="server" BorderColor="#DEDFDE" BorderStyle="Double" BorderWidth="1px">
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImgEditar" CommandArgument='<%#Eval("fac_codigo") %>' CommandName="Editar" CausesValidation="false" ToolTip="Editar datos"
                                                Height="40px" Width="40px" ImageUrl="~/Images/Edit.png" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Factura">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:Label ID="Lblaero" runat="server" Text='<%#Eval("fac_numero") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Estado">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblnomen" runat="server" Text='<%#Eval("fac_estado") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Forma de pago">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblciu" runat="server" Text='<%#Eval("TBL_FORMA_PAGO.formp_descripcion") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Neto">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblnet" runat="server" Text='<%#Eval("fac_neto") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Comisión">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblccom" runat="server" Text='<%#Eval("fac_comision") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Iva">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbliv" runat="server" Text='<%#Eval("fac_iva") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Total">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbltot" runat="server" Text='<%#Eval("fac_total") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
