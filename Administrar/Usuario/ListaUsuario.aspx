﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaginaMaestra/Lista.Master" AutoEventWireup="true" CodeBehind="ListaUsuario.aspx.cs" Inherits="PegasoTravel.Administrar.Usuario.ListaUsuario" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cph_cabecera" runat="server">
    <link href="../../CSS/styles.css" rel="stylesheet" />
    <script lang="jscript" type="text/javascript">
        function Seleccion(idusu, Nombre, Apellido) {
            var usuario = {};
            usuario.idusu = idusu;
            usuario.Nombre = Nombre;
            usuario.Apellido = Apellido;
            window.returnValue = usuario;
            window.close();
        }
    </script>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_contenido" runat="server">
 <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table border="0" align="center"  >
                <tr>
                    <td >
                        <asp:Label ID="LblBusqueda" runat="server" Text="Buscar por:" ForeColor="#122931" Font-Size="12px"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="DdlBusqueda" runat="server" ForeColor="#122931" Font-Size="12px" ToolTip="Seleccionar tipo de búsqueda">
                            <asp:ListItem Text="Seleccione Busqueda" Value="0"></asp:ListItem>
                            <asp:ListItem Text="Apellido" Value="a"></asp:ListItem>
                            <asp:ListItem Text="Cedula" Value="c"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:TextBox ID="TxtBusqueda" runat="server" Width="140px" CausesValidation="false" ForeColor="#122931" Font-Size="12px"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="TxtBusqueda" FilterType="Numbers, Custom, UppercaseLetters, LowercaseLetters" ValidChars="- _" runat="server"></ajaxToolkit:FilteredTextBoxExtender>
                          </td>
                    <td>
                        <asp:ImageButton ID="ImgBusqueda" ImageUrl="~/Images/buscar.png" OnClick="ImgBusqueda_click" runat="server" ToolTip="Buscar" Height="40px" Width="40px" />
                    </td>
                </tr>
            </table>
            <div style="overflow: auto; height: 200px;width:auto">
                <table border="0" align="center">
                    <tr>
                        <td align="center">
                            <asp:GridView ID="Gdv_Lista" AutoGenerateColumns="false"
                                runat="server" BorderColor="#DEDFDE" BorderStyle="Double" BorderWidth="1px">
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgSelecionar" runat="server" CausesValidation="false" ToolTip="Seleccionar" CommandName="Seleccionar" Height="40px" Width="40px"
                                                ImageUrl="~/Images/select.png" OnClientClick='<%# "Seleccion (" + Eval("usu_codigo") + ", \"" + Eval("usu_nombre") + "\", \"" + Eval("usu_apellido") +  "\" )" %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Nombre">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:Label ID="LblNombre" runat="server" Text='<%#Eval("usu_nombre") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Apellido">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:Label ID="LblApellido" runat="server" Text='<%#Eval("usu_apellido") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
