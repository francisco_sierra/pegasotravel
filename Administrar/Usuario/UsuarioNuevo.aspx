﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaginaMaestra/Admin.Master" AutoEventWireup="true" CodeBehind="UsuarioNuevo.aspx.cs" Inherits="PegasoTravel.Administrar.Usuario.UsuarioNuevo" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cph_cabecera" runat="server">
    <link href="../../CSS/styles.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_contenido" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table style="text-align: center" border="0" align="center" class="auto-style5">
                <tr>
                    <td>
                        <asp:Label ID="Label2" runat="server" Text="USUARIO" Font-Size="24px" Font-Bold="true" ForeColor="#122931"></asp:Label>
                        <br />
                        <br />
                    </td>
                </tr>
            </table>
            <table style="width: 100%; text-align: center" border="0" align="center">
                <tr>
                    <td align="left">
                        <asp:Label ID="LblMensaje" runat="server" ForeColor="Red" Font-Size="14px" Font-Bold="true"></asp:Label>
                    </td>
                    <td align="right">
                        <asp:HiddenField ID="HdfUsu" runat="server" />
                    </td>
                </tr>
            </table>
            <table style="width: 100%" border="0" align="center">
                <tr>
                    <td align="center">
                        <asp:ImageButton ID="ImgNuevoUsuario" ImageUrl="~/Images/new.png" Width="40px" Height="40px" ToolTip="Agregar nuevo usuario" runat="server" OnClick="ImgNuevoUsuario_Click" CausesValidation="false" />
                        <br />
                        <asp:Label ID="Label1" runat="server" Text="Agregar Usuario" Font-Bold="true" ForeColor="#122931" Font-Size="14px"></asp:Label>
                    </td>
                </tr>
            </table>
            <table border="0" align="center">
                <tr>
                    <td>
                        <asp:Label ID="LblNombre" runat="server" Text="Nombre:" ForeColor="#122931" Font-Size="12px"></asp:Label>
                    </td>
                    <td class="style14">
                        <asp:TextBox ID="TxtNombre" runat="server" Width="140px" ForeColor="#122931" Font-Size="12px"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="TxtNombre" FilterType="Custom, UppercaseLetters, LowercaseLetters" ValidChars="- _" runat="server"></ajaxToolkit:FilteredTextBoxExtender>
                    </td>
                    <td>
                        <asp:Label ID="LblApellido" runat="server" Text="Apellido:" ForeColor="#122931" Font-Size="12px"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="TxtApellido" runat="server" Width="140px" ForeColor="#122931" Font-Size="12px"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" TargetControlID="TxtApellido" FilterType="Numbers, Custom, UppercaseLetters, LowercaseLetters" ValidChars="- _" runat="server"></ajaxToolkit:FilteredTextBoxExtender>
                    </td>
                    <td>
                        <asp:Label ID="LblCedula" runat="server" Text="Cédula:" ForeColor="#122931" Font-Size="12px"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="TxtCedula" runat="server" Width="140px" ForeColor="#122931" Font-Size="12px"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" TargetControlID="TxtCedula" FilterType="Numbers" runat="server"></ajaxToolkit:FilteredTextBoxExtender>
                        <br />
                        <asp:RadioButton ID="RbDocumento" runat="server" Text="Pasaporte" ForeColor="#122931" Font-Size="12px" ToolTip="Seleccionar en caso de no tener Cédula ecuatoriana" />
                    </td>
                    <tr>
                        <td>
                            <asp:Label ID="LblDireccion" runat="server" Text="Dirección:" ForeColor="#122931" Font-Size="12px"></asp:Label>

                        </td>
                        <td>
                            <asp:TextBox ID="TxtDireccion" runat="server" Width="140px" ForeColor="#122931" Font-Size="12px"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtend" TargetControlID="TxtDireccion" FilterType="Numbers, Custom, UppercaseLetters, LowercaseLetters" ValidChars="- _" runat="server"></ajaxToolkit:FilteredTextBoxExtender>
                     
                             </td>
                        <td>
                            <asp:Label ID="LblTelefono" runat="server" Text="Teléfono:" ForeColor="#122931" Font-Size="12px"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="TxtTefelono" runat="server" Width="140px" ForeColor="#122931" Font-Size="12px"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" TargetControlID="TxtTefelono" FilterType="Numbers" runat="server"></ajaxToolkit:FilteredTextBoxExtender>
                        </td>
                        <td>
                            <asp:Label ID="LblEmail" runat="server" Text="E-mail:" ForeColor="#122931" Font-Size="12px"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="TxtEmail" runat="server" Width="140px" ForeColor="#122931" Font-Size="12px"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" TargetControlID="TxtEmail" FilterType="LowercaseLetters,Custom,Numbers" ValidChars=".@_" runat="server"></ajaxToolkit:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="LblTipoUsuario" runat="server" Text="Perfil:" ForeColor="#122931" Font-Size="12px"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="Ddl_tipo_usuario" runat="server" Width="140px" ForeColor="#122931" Font-Size="12px" ToolTip="Seleccionar el perfil del usuario">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td align="right">
                            <asp:Button ID="BtnGuardar" runat="server" OnClick="BtnGuardar_Click" BackColor="#122931"
                                ForeColor="White" Height="37px" Width="130px" Font-Bold="true" Font-Size="18px" Text="Guardar" />
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td align="right">
                            <asp:Button ID="BtnModificar" runat="server" OnClick="BtnModificar_Click" Text="Modificar" BackColor="#122931"
                                ForeColor="White" Height="37px" Width="130px" Font-Bold="true" Font-Size="18px" />
                        </td>
                    </tr>
                </tr>
            </table>
            <br />
            <br />
            <table border="0" align="center">
                <tr>
                    <td>
                        <asp:Label ID="LblBusqueda" runat="server" Text="Buscar usuario" Font-Bold="true" ForeColor="#122931" Font-Size="16px"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label3" runat="server" Text="Ingrese Apellido: " ForeColor="#122931" Font-Size="12px"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="TxtBusqueda" runat="server" Width="140px" CssClass="auto-style3" ForeColor="#122931" Font-Size="12px"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" TargetControlID="TxtBusqueda" FilterType="Custom, UppercaseLetters, LowercaseLetters" ValidChars="- _" runat="server"></ajaxToolkit:FilteredTextBoxExtender>
                    </td>
                    <td>
                        <asp:ImageButton ID="ImgBusqueda" ImageUrl="~/Images/buscar.png" runat="server" Height="40px" Width="40px" ToolTip="Buscar" CausesValidation="false" OnClick="ImgBusqueda_Click" />
                    </td>
                </tr>
            </table>
            <div style="overflow: auto; height: 200px; width: auto">
                <table align="center" border="0" width="100%">

                    <tr>
                        <td align="center">
                            <asp:GridView ID="Gdv_ListaUsuario" AutoGenerateColumns="false" OnRowCommand="UsuarioRowCommand"
                                runat="server" BorderColor="#DEDFDE" BorderStyle="Double" BorderWidth="1px">
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImgEditar" CommandArgument='<%#Eval("usu_codigo") %>' CommandName="Editar" CausesValidation="false" ToolTip="Editar datos"
                                                Height="40px" Width="40px" ImageUrl="~/Images/Edit.png" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" />
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImgEliminar" CommandArgument='<%#Eval("usu_codigo") %>' CommandName="Eliminar" CausesValidation="false" ToolTip="Desactivar usuario"
                                                Height="40px" Width="40px" ImageUrl="~/Images/delete.png" runat="server" OnClientClick="return confirm('Seguro que desea desactivar el usuario?')" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Cedula">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:Label ID="LblCedula" runat="server" Text='<%#Eval("usu_cedula") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Usuario">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:Label ID="LblNombre" runat="server" Text='<%#Eval("usu_nombre") +" "+Eval("usu_apellido") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Dirección">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:Label ID="LblDireccion" runat="server" Text='<%#Eval("Usu_direccion") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Telefono">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:Label ID="LblTelefono" runat="server" Text='<%#Eval("usu_telefono") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="E-mail">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:Label ID="LblEmail" runat="server" Text='<%#Eval("usu_email") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Perfil">
                                        <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                        <ItemTemplate>
                                            <asp:Label ID="LblCargo" runat="server" Text='<%#Eval("TBL_TIPO_USUARIO.tusu_descripcion") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>

                </table>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
