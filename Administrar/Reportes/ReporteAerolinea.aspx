﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaginaMaestra/Admin.Master" AutoEventWireup="true" CodeBehind="ReporteAerolinea.aspx.cs" Inherits="PegasoTravel.Administrar.Reportes.ReporteAerolinea" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cph_cabecera" runat="server">

    <link href="../../CSS/styles.css" rel="stylesheet" />
     <link href="../../CSS/bootstrap.css" rel="stylesheet" />
    <style type="text/css">
        .modalBackground {
            background-color: Black;
            filter: alpha(opacity=90);
            opacity: 0.8;
            z-index: 10000;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_contenido" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table style="width: 100%; text-align: center" border="0" align="center">
                <tr>
                    <td>
                        <asp:Label ID="Label2" runat="server" Text="REPORTE AEROLÍNEA" Font-Size="24px" Font-Bold="true" ForeColor="#122931"></asp:Label>
                        <br />
                        <br />
                    </td>
                </tr>
            </table>
            <table style="width: 100%; text-align: center" border="0" align="center">
                <tr>
                    <td align="left">
                        <asp:Label ID="LblMensaje" runat="server" ForeColor="Red" Font-Size="12px" Font-Bold="true"></asp:Label>
                    </td>
                    <td align="right">
                        <asp:HiddenField ID="HdfUsu" runat="server" />
                    </td>
                </tr>
            </table>
            <table align="center" border="0">
                <tr>
                    <td>
                        <asp:Label ID="LblFechaSalida" runat="server" Text="Desde:" ForeColor="#122931" Font-Size="12px"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="TxtFechaSalida" runat="server" Width="140px" ForeColor="#122931" Font-Size="12px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:ImageButton ID="ImgFechaSalida" ImageUrl="~/Images/calendar.png" CausesValidation="false" runat="server" ToolTip="Seleccionar fecha de inicio" Height="40px" Width="40px" />
                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" Format="dd/MM/yyyy" PopupPosition="TopRight" PopupButtonID="ImgFechaSalida" TargetControlID="TxtFechaSalida" runat="server"></ajaxToolkit:CalendarExtender>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LblFechaRetorno" runat="server" Text="Hasta:" ForeColor="#122931" Font-Size="12px"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="TxtFechaRetorno" runat="server" Width="140px" ForeColor="#122931" Font-Size="12px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:ImageButton ID="ImgFechaRetorno" ImageUrl="~/Images/calendar.png" CausesValidation="false" runat="server" ToolTip="Seleccionar fecha final" Height="40px" Width="40px" />
                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" TargetControlID="TxtFechaRetorno" PopupPosition="TopRight"  Format="dd/MM/yyyy" PopupButtonID="ImgFechaRetorno" runat="server"></ajaxToolkit:CalendarExtender>
                       
                         </td>
                </tr>
            </table>
            <br />
            <br />
            <br />
            <br />
            <table border="0" align="center">
                <tr>
                    <td>
                        <rsweb:ReportViewer ID="ReportViewer1" BackColor="White" runat="server" Font-Names="Verdana" ForeColor="#122931" Height="100%" Width="100%" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" ShowBackButton="False" ShowFindControls="False" ShowPageNavigationControls="False" ShowPrintButton="False" SizeToReportContent="True">
                            <LocalReport ReportPath="Administrar\Reportes\Aerolinea.rdlc">
                                <DataSources>
                                    <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="DstAerolinea" />
                                </DataSources>
                            </LocalReport>
                        </rsweb:ReportViewer>

                        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="PegasoTravel.Administrar.Reportes.DSClienteTableAdapters.sp_ReservaXAerolineaTableAdapter">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="TxtFechaSalida" Name="fecha1" PropertyName="Text" Type="DateTime" />
                                <asp:ControlParameter ControlID="TxtFechaRetorno" Name="fecha2" PropertyName="Text" Type="DateTime" />
                            </SelectParameters>
                        </asp:ObjectDataSource>

                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
