﻿
<%@ Page Title="" Language="C#" MasterPageFile="~/PaginaMaestra/Admin.Master" AutoEventWireup="true" CodeBehind="ReporteCliente.aspx.cs" Inherits="PegasoTravel.Administrar.Reportes.ReporteCliente" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cph_cabecera" runat="server">

    <link href="../../CSS/styles.css" rel="stylesheet" />
     <link href="../../CSS/bootstrap.css" rel="stylesheet" />
    <style type="text/css">
        .modalBackground {
            background-color: Black;
            filter: alpha(opacity=90);
            opacity: 0.8;
            z-index: 10000;
        }
    </style>
    <script src="../../JS/Functions.js"></script>
   <%-- <script language="javascript" type="text/javascript">
        function ven_cliente() {
            var url = "../../Administrar/Cliente/ListaCliente.aspx?window=1";
            var cliente = openDialogWindow(url, 600, 400);
            if (cliente != null) {
                var idcli = $("<%=hdf_idcli.ClientID%>");
                var Nombre = $("<%=TxtNomCliente.ClientID%>");
                var Apellido = $("<%=TxtApeCliente.ClientID%>");
                idcli.value = cliente.idcli;
                Nombre.value = cliente.Nombre;
                Apellido.value = cliente.Apellido;
                Telefono1.value = cliente.Telefono1;
            }

            return false;
        }
    </script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_contenido" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table style="width: 100%; text-align: center" border="0" align="center">
                <tr>
                    <td>
                        <asp:Label ID="Label2" runat="server" Text="REPORTE CLIENTE" Font-Size="24px" Font-Bold="true" ForeColor="#122931"></asp:Label>
                        <br />
                        <br />
                    </td>
                </tr>
            </table>
            <table style="width: 100%; text-align: center" border="0" align="center">
                <tr>
                    <td align="left">
                        <asp:Label ID="LblMensaje" runat="server" ForeColor="Red" Font-Size="14px" Font-Bold="true"></asp:Label>
                    </td>
                    <td align="right">
                        <asp:HiddenField ID="HdfUsu" runat="server" />
                    </td>
                </tr>
            </table>
            <table align="center" border="0">
                <tr>
                    <td>
                        <asp:Label ID="LblCli" runat="server" Text="Cliente:" ForeColor="#122931" Font-Size="12px"></asp:Label>
                    </td>
                   <%-- <td>
                        <asp:TextBox ID="TxtNomCliente" runat="server" Width="140px" ForeColor="#122931" Font-Size="12px"></asp:TextBox>
                    </td>--%>
                    <td>
                        <asp:TextBox ID="TxtApeCliente" runat="server" Width="140px" ForeColor="#122931" Font-Size="12px"></asp:TextBox>
                    </td>

                    <td>
                        <asp:ImageButton ID="ImgCliente" runat="server" CausesValidation="false" Height="40px" ImageUrl="~/Images/buscar.png" ToolTip="Buscar" OnClick="ImgCliente_click"  />
                                   
                        
                                   
                         <asp:HiddenField ID="hdf_idcli" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LblFechaSalida" runat="server" Text="Desde:" ForeColor="#122931" Font-Size="12px"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="TxtFechaSalida" runat="server" Width="140px" ForeColor="#122931" Font-Size="12px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:ImageButton ID="ImgFechaSalida" ImageUrl="~/Images/calendar.png" CausesValidation="false" ToolTip="Seleccionar fecha de inicio" runat="server" Height="40px" Width="40px" />
                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" Format="dd/MM/yyyy" PopupPosition="TopRight" PopupButtonID="ImgFechaSalida" TargetControlID="TxtFechaSalida" runat="server"></ajaxToolkit:CalendarExtender>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LblFechaRetorno" runat="server" Text="Hasta:" ForeColor="#122931" Font-Size="12px"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="TxtFechaRetorno" runat="server" Width="140px" ForeColor="#122931" Font-Size="12px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:ImageButton ID="ImgFechaRetorno" ImageUrl="~/Images/calendar.png" CausesValidation="false" runat="server" ToolTip="Seleccionar fecha final" Height="40px" Width="40px" />
                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" TargetControlID="TxtFechaRetorno" PopupPosition="TopRight" Format="dd/MM/yyyy" PopupButtonID="ImgFechaRetorno" runat="server"></ajaxToolkit:CalendarExtender>
                    </td>
                </tr>
            </table>
            <br />
            <br />
            <br />
            <br />
            <table border="0" align="center">
                <tr>
                    <td >
                        <rsweb:ReportViewer ID="ReportViewer1" BackColor="White" runat="server" ForeColor="#122931" Font-Names="Verdana" Height="100%" Width="100%" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" ShowBackButton="False" ShowFindControls="False" ShowPageNavigationControls="False" ShowPrintButton="False" SizeToReportContent="True">
                            <LocalReport ReportPath="Administrar\Reportes\Cliente.rdlc">
                                <DataSources>
                                    <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="DstCliente" />
                                </DataSources>
                            </LocalReport>
                        </rsweb:ReportViewer>

                        <asp:ObjectDataSource ID="ObjectDataSource1"  runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="PegasoTravel.Administrar.Reportes.DSClienteTableAdapters.sp_ReservaXClienteTableAdapter">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="hdf_idcli" Name="idcli" PropertyName="Value" Type="Int32" />
                                <asp:ControlParameter ControlID="TxtFechaSalida" Name="fecha1" PropertyName="Text" Type="DateTime" />
                                <asp:ControlParameter ControlID="TxtFechaRetorno" Name="fecha2" PropertyName="Text" Type="DateTime" />
                            </SelectParameters>
                        </asp:ObjectDataSource>

                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
     <asp:Button ID="btnInicial" runat="server" Text="Button" Style="display: none" />
     <ajaxToolkit:ModalPopupExtender ID="ImgCliente_ModalPopupExtender" BackgroundCssClass="modalBackground" PopupControlID="PanelModal"
        runat="server" DynamicServicePath="" Enabled="True" TargetControlID="btnInicial">
    </ajaxToolkit:ModalPopupExtender>  

      <asp:Panel ID="PanelModal" runat="server" Style=" display: none; width: 40%;background:white ; height: auto">
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <table border="0" style="width: 100%">
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="ImgSalir" runat="server" ImageUrl="~/Images/delete.png" Width="30px" Height="30px" OnClick="ImgSalir_Click" />
                        </td>
                    </tr>
                </table>
                <table border="0" align="center">
                    <tr>
                        <td>
                            <asp:Label ID="Label3" runat="server" Text="Buscar por:" ForeColor="#122931" Font-Size="12px"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="DropDownList1" runat="server" ForeColor="#122931" Font-Size="12px" ToolTip="Seleccionar el tipo de búsqueda">
                                <asp:ListItem Text="Seleccione Busqueda" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Apellido" Value="a"></asp:ListItem>
                                <asp:ListItem Text="Cedula" Value="c"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <asp:TextBox ID="TxtBusca" runat="server" Width="140px" ForeColor="#122931" Font-Size="12px" CausesValidation="false"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" TargetControlID="TxtBusca" FilterType="Numbers, Custom, UppercaseLetters, LowercaseLetters" ValidChars="- _" runat="server"></ajaxToolkit:FilteredTextBoxExtender>
                        </td>
                        <td>
                            <asp:ImageButton ID="ImgBuscaCli" ImageUrl="~/Images/buscar.png" OnClick="ImgBuscaCli_click" runat="server" Height="40px" Width="40px" ToolTip="Buscar" />
                        </td>
                    </tr>
                </table>
                <div style="overflow: auto; height: 200px; width: auto">
                    <table border="0" align="center">
                        <tr>
                            <td align="center">
                                <asp:GridView ID="GdvCliente" AutoGenerateColumns="false" OnRowCommand="Cliente"
                                    runat="server" BorderColor="#DEDFDE" BorderStyle="Double" BorderWidth="1px">
                                    <Columns>
                                        <asp:TemplateField>
                                            <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgSelecionar" runat="server" CausesValidation="false" CommandName="Seleccionar" CommandArgument='<%#Eval("cli_codigo") %>' ToolTip="Seleccionar" Height="40px" Width="40px"
                                                    ImageUrl="~/Images/select.png" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Nombre">
                                            <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                            <ItemTemplate>
                                                <asp:Label ID="LblNombre" runat="server" Text='<%#Eval("cli_nombre") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Apellido">
                                            <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                            <ItemTemplate>
                                                <asp:Label ID="LblAp" runat="server" Text='<%#Eval("cli_apellido") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Cédula">
                                            <HeaderStyle BackColor="#122931" Font-Bold="True" ForeColor="White" Font-Size="12px" />
                                            <ItemTemplate>
                                                <asp:Label ID="Lbldoc" runat="server" Text='<%#Eval("cli_documento") %>' ForeColor="#122931" Font-Size="12px"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </div>
            </ContentTemplate>

        </asp:UpdatePanel>
    </asp:Panel>


</asp:Content>
