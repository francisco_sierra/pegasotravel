﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaginaMaestra/Admin.Master" AutoEventWireup="true" CodeBehind="ReportePresentacion.aspx.cs" Inherits="PegasoTravel.Administrar.Reportes.ReportePresentacion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cph_cabecera" runat="server">
    <link href="../../CSS/styles.css" rel="stylesheet" />
    <style type="text/css">
        .auto-style6 {
            width: 200px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_contenido" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table style="width: 100%; text-align: center" border="0" align="center">
                <tr>
                    <td>
                        <asp:Label ID="Label3" runat="server" Text="REPORTES" Font-Size="24px" Font-Bold="true" ForeColor="#122931"></asp:Label>
                        <br />
                        <br />
                    </td>
                </tr>
            </table>
            <br />     <br />     <br />    

            <table align="center">
                <tr>
                  
                    <td  align="center" class="auto-style6" >
                        <asp:ImageButton ID="ImgClienteReporte" ImageUrl="~/Images/report.png" Height="80px" Width="80px" ToolTip="Reporte de cliente" runat="server" OnClick="ImgClienteReporte_Click" />
                   <br />
                        <asp:Label ID="LblReporteCliente" runat="server" Text="Reporte de Cliente" Font-Bold="true" ForeColor="#122931" Font-Size="12px"></asp:Label>
                         </td>
                     <td  align="center" class="auto-style6">
                          <asp:ImageButton ID="ImgUsuarioReporte" ImageUrl="~/Images/report.png" Height="80px" Width="80px" runat="server" ToolTip="Reporte de usuario" OnClick="ImgUsuarioReporte_Click" />
                   <br />
                        <asp:Label ID="Label1" runat="server" Text="Reporte de Usuario" Font-Bold="true" ForeColor="#122931" Font-Size="12px"></asp:Label>
                    </td>
                      <td  align="center" class="auto-style6">
                          <asp:ImageButton ID="ImgAerolineaReporte" ImageUrl="~/Images/reporte.png" Height="80px" Width="80px" runat="server" ToolTip="Reporte de aerolínea" OnClick="ImgAerolineaReporte_Click" />
                   <br />
                        <asp:Label ID="Label2" runat="server" Text="Reporte de Aerolínea" Font-Bold="true" ForeColor="#122931" Font-Size="12px"></asp:Label>
                    </td>
                  
                </tr>
            </table>

            <br />
            <br />
            <br />
            <br />
            <br />



        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
